<?php
namespace App\Mapper;
use Sys\Mapper\Mapper;

class ResidentMapper extends Mapper{

  protected $_table = 'tbl_resident';



  public function selectDataTable($filter, $columns, $limit, $offset, $order, $add_filter = array()){
    $result = array(
      'data'  => array()
    , 'total_count'=>0
    , 'count'=>0
    );

    $order_str_query = "ORDER BY ";
    $limit_str_query = "LIMIT :limit OFFSET :offset";
    $column_str_query = "";
    $group_str_query = " GROUP BY resident_id, brgy_name, resident, bc_gender, age, resident_nationality, resident_civil_status, bc_phone_num1, bc_phone_num2 ";
    $where_str_query = "";
    $params = array();


    $column_str_query = ' resident_id, brgy_name,
                          CONCAT(bc_last_name, \', \', bc_first_name, \' \', bc_middle_name, \' \', bc_name_ext) as resident,
                          bc_gender, TIMESTAMPDIFF(YEAR, resident_birthdate, CURDATE()) AS age,
                          resident_nationality, resident_civil_status,
                          bc_phone_num1, bc_phone_num2, SUM(CASE
                                                            	WHEN `complaint_is_resolved` = \'0\' THEN 1
                                                            	ELSE 0
                                                            END) as `legal_case` ';
    if(!empty($filter)){
        $where_str_query .= " (bc_first_name LIKE :bc_first_name OR bc_middle_name LIKE :bc_middle_name
                            OR bc_last_name LIKE :bc_last_name OR bc_name_ext LIKE :bc_name_ext OR
                            brgy_name LIKE :brgy_name OR bc_gender LIKE :bc_gender OR TIMESTAMPDIFF(YEAR, resident_birthdate, CURDATE()) LIKE :age
                            OR resident_nationality LIKE :resident_nationality OR resident_civil_status LIKE :resident_civil_status
                            OR bc_phone_num1 LIKE :bc_phone_num1 OR bc_phone_num2 LIKE :bc_phone_num2) ";
        $params[':bc_first_name'] = '%'.$filter.'%';
        $params[':bc_middle_name'] = '%'.$filter.'%';
        $params[':bc_last_name'] = '%'.$filter.'%';
        $params[':bc_name_ext'] = '%'.$filter.'%';
        $params[':brgy_name'] = '%'.$filter.'%';
        $params[':bc_gender'] = '%'.$filter.'%';
        $params[':age'] = '%'.$filter.'%';
        $params[':resident_nationality'] = '%'.$filter.'%';
        $params[':resident_civil_status'] = '%'.$filter.'%';
        $params[':bc_phone_num1'] = '%'.$filter.'%';
        $params[':bc_phone_num2'] = '%'.$filter.'%';
    }

    if(!empty($add_filter)){
      if($add_filter['brgy'] != '0'){
        if(!empty($where_str_query)){
          $where_str_query .= ' AND ';
        }
        $where_str_query .= " brgy_id = '".$add_filter['brgy']."' ";
      }
    }

    foreach($order as $i=>$_order){
      $order_str_query .= $_order['col']." ".$_order['type'];
      if(next($order)){
        $order_str_query .= ", ";
      }
    }

    if(strlen($where_str_query)>0){
      $where_str_query = ' WHERE '.$where_str_query;
    }

    $sql_statement = "SELECT COUNT(1) as 'num'
                      FROM `tbl_resident`
                      INNER JOIN `tbl_basic_contact`
                      ON `resident_bc_id` = `bc_id`
                      INNER JOIN `tbl_address`
                      ON `resident_address_id` = `address_id`
                      LEFT JOIN `tbl_barangay`
                      ON `address_brgy_id` = `brgy_id`
                      LEFT JOIN `tbl_complaint`
                      ON `complaint_resident_id` = `resident_id`" . $where_str_query ;
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result['count'] = $stmt->fetch(\PDO::FETCH_ASSOC)['num'];

    $sql_statement = "SELECT ".$column_str_query."
                      FROM `tbl_resident`
                      INNER JOIN `tbl_basic_contact`
                      ON `resident_bc_id` = `bc_id`
                      INNER JOIN `tbl_address`
                      ON `resident_address_id` = `address_id`
                      LEFT JOIN `tbl_barangay`
                      ON `address_brgy_id` = `brgy_id`
                      LEFT JOIN `tbl_complaint`
                      ON `complaint_resident_id` = `resident_id` " . $where_str_query . " ".$group_str_query.'' . $order_str_query. " ".$limit_str_query;
		$stmt = $this->prepare($sql_statement);
    $params[':limit'] = $limit;
    $params[':offset'] = $offset;

		$stmt->execute($params);
		$result['data'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);


    $result['total_count'] = $this->getAllCount();

		return $result;
  }

  public function checkIfRegistered($data, $brgy_id){
    $params = array(
      ':bc_first_name'=>$data['bc_first_name']
    , ':bc_middle_name'=>$data['bc_middle_name']
    , ':bc_last_name'=>$data['bc_last_name']
    , ':bc_name_ext'=>$data['bc_name_ext']
    , ':bc_gender'=>$data['bc_gender']
    , ':resident_birthdate'=>date('Y-m-d', strtotime($data['resident_birthdate']))
    );
    $brgy_statement = '';
    if($brgy_id > 0){
      $params[':address_brgy_id'] = $brgy_id;
      $brgy_statement.=' address_brgy_id = :address_brgy_id ';
    }
    $sql_statement = "SELECT *
                      FROM `tbl_resident`
                      INNER JOIN `tbl_basic_contact`
                      ON `resident_bc_id` = `bc_id`
                      INNER JOIN `tbl_address`
                      ON `resident_address_id` = `address_id`
                      WHERE UPPER(`bc_first_name`) = UPPER(:bc_first_name)
                      AND UPPER(`bc_middle_name`) = UPPER(:bc_middle_name)
                      AND UPPER(`bc_last_name`) = UPPER(:bc_last_name)
                      AND UPPER(`bc_name_ext`) LIKE :bc_name_ext
                      AND UPPER(`bc_gender`) = UPPER(:bc_gender)
                      AND `resident_birthdate` = :resident_birthdate ".$brgy_statement;

		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  /*
  SELECT resident_id, brgy_name, bc_last_name + ', ' + bc_first_name + ' ' + bc_middle_name + ' ' + bc_name_ext as `resident`, bc_gender, TIMESTAMPDIFF(YEAR, resident_birthdate, CURDATE()) AS age, resident_nationality, resident_civil_status, bc_phone_num1, bc_phone_num2, '0'
FROM `tbl_resident`
INNER JOIN `tbl_basic_contact`
ON `resident_bc_id` = `bc_id`
INNER JOIN `tbl_address`
ON `resident_address_id` = `address_id`
LEFT JOIN `tbl_barangay`
ON `address_brgy_id` = `brgy_id`




  */
}
