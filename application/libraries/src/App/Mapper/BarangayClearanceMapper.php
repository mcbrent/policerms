<?php
namespace App\Mapper;
use Sys\Mapper\Mapper;

class BarangayClearanceMapper extends Mapper{

  protected $_table = 'tbl_barangay_clearance';

  public function selectDataTable($filter, $columns, $limit, $offset, $order){
    $result = array(
      'data'  => array()
    , 'total_count'=>0
    , 'count'=>0
    );

    $order_str_query = "";
    $group_str_query = "";
    $limit_str_query = "LIMIT :limit OFFSET :offset";
    $column_str_query = "";
    $where_str_query = "";
    $params = array();

    $column_str_query = ' brgyc_id, CONCAT(bc_last_name, \', \', bc_first_name, \' \', bc_middle_name, \' \', bc_name_ext) as resident, brgy_name,
                        brgyc_validity_from, brgyc_validity_to,
                        (CASE
                        	WHEN (CURRENT_DATE()+INTERVAL 1 DAY) BETWEEN brgyc_validity_from AND brgyc_validity_to THEN \'1\'
                            ELSE \'0\'
                        END) as \'validity\'';
    if(!empty($filter)){
        $where_str_query .= " (bc_first_name LIKE :bc_first_name OR bc_middle_name LIKE :bc_middle_name
                            OR bc_last_name LIKE :bc_last_name OR bc_name_ext LIKE :bc_name_ext OR
                            brgy_name LIKE :brgy_name ) ";
        $params[':bc_first_name'] = '%'.$filter.'%';
        $params[':bc_middle_name'] = '%'.$filter.'%';
        $params[':bc_last_name'] = '%'.$filter.'%';
        $params[':bc_name_ext'] = '%'.$filter.'%';
        $params[':brgy_name'] = '%'.$filter.'%';
    }

    foreach($order as $i=>$_order){
      $order_str_query .= $_order['col']." ".$_order['type'];
      if(next($order)){
        $order_str_query .= ", ";
      }
    }

    if(strlen($where_str_query)>0){
      $where_str_query = ' WHERE '.$where_str_query;
    }
    if(strlen($order_str_query)>0){
      $order_str_query = ' ORDER BY '.$order_str_query;
    }

    $sql_statement = "SELECT COUNT(1) as 'num'
                      FROM `tbl_barangay_clearance`
                      INNER JOIN `tbl_resident`
                      ON `brgyc_resident_id` = `resident_id`
                      INNER JOIN `tbl_basic_contact`
                      ON `resident_bc_id` = `bc_id`
                      INNER JOIN `tbl_address`
                      ON `resident_address_id` = `address_id`
                      INNER JOIN `tbl_barangay`
                      ON `address_brgy_id` = `brgy_id`" . $where_str_query;
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result['count'] = $stmt->fetch(\PDO::FETCH_ASSOC)['num'];

    $sql_statement = "SELECT ".$column_str_query."
                      FROM `tbl_barangay_clearance`
                      INNER JOIN `tbl_resident`
                      ON `brgyc_resident_id` = `resident_id`
                      INNER JOIN `tbl_basic_contact`
                      ON `resident_bc_id` = `bc_id`
                      INNER JOIN `tbl_address`
                      ON `resident_address_id` = `address_id`
                      INNER JOIN `tbl_barangay`
                      ON `address_brgy_id` = `brgy_id` " . $where_str_query . " ". $group_str_query . " " . $order_str_query. " ".$limit_str_query;
		$stmt = $this->prepare($sql_statement);
    $params[':limit'] = $limit;
    $params[':offset'] = $offset;

		$stmt->execute($params);
		$result['data'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);


    $result['total_count'] = $this->getAllCount();

		return $result;
  }


  public function checkExistByResident($brgyc_resident_id){
    $params = array(
      ':brgyc_resident_id'=>$brgyc_resident_id
    );
    $sql_statement = "SELECT *
                      FROM `tbl_barangay_clearance`
                      WHERE CURRENT_DATE() BETWEEN brgyc_validity_from AND 	brgyc_validity_to AND `brgyc_resident_id` = :brgyc_resident_id ";

		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }


}
