<?php
namespace App\Mapper;
use Sys\Mapper\Mapper;

class PCRequestMapper extends Mapper{

  protected $_table = 'tbl_pc_request';

  public function selectDataTable($filter, $columns, $limit, $offset, $order){
    $result = array(
      'data'  => array()
    , 'total_count'=>0
    , 'count'=>0
    );

    $order_str_query = "ORDER BY ";
    $group_str_query = "GROUP BY resident, bc_gender, age, pcq_purpose, brgy_name, pcq_status, pcq_no, pcq_id";
    $limit_str_query = "LIMIT :limit OFFSET :offset";
    $column_str_query = "";
    $where_str_query = "";
    $params = array();

    $column_str_query = ' CONCAT(bc_last_name, \', \', bc_first_name, \' \', bc_middle_name, \' \', bc_name_ext) as resident,
                        bc_gender, TIMESTAMPDIFF(YEAR, resident_birthdate, CURDATE()) AS age,
                        pcq_purpose, brgy_name, pcq_status, pcq_no, pcq_id, SUM(case when complaint_is_resolved = 0 then 1 else 0 end) complaint';
    if(!empty($filter)){
        $where_str_query .= " pcq_status IN (0,1) AND (bc_first_name LIKE :bc_first_name OR bc_middle_name LIKE :bc_middle_name
                            OR bc_last_name LIKE :bc_last_name OR bc_name_ext LIKE :bc_name_ext OR
                            brgy_name LIKE :brgy_name OR bc_gender LIKE :bc_gender OR TIMESTAMPDIFF(YEAR, resident_birthdate, CURDATE()) LIKE :age
                            OR pcq_purpose LIKE :pcq_purpose OR pcq_no LIKE :pcq_no) ";
        $params[':bc_first_name'] = '%'.$filter.'%';
        $params[':bc_middle_name'] = '%'.$filter.'%';
        $params[':bc_last_name'] = '%'.$filter.'%';
        $params[':bc_name_ext'] = '%'.$filter.'%';
        $params[':brgy_name'] = '%'.$filter.'%';
        $params[':bc_gender'] = '%'.$filter.'%';
        $params[':age'] = '%'.$filter.'%';
        $params[':pcq_no'] = '%'.$filter.'%';
        $params[':pcq_purpose'] = '%'.$filter.'%';
    }

    foreach($order as $i=>$_order){
      $order_str_query .= $_order['col']." ".$_order['type'];
      if(next($order)){
        $order_str_query .= ", ";
      }
    }

    if(strlen($where_str_query)>0){
      $where_str_query = ' WHERE '.$where_str_query;
    }

    $sql_statement = "SELECT COUNT(1) as 'num'
                      FROM `tbl_pc_request`
                      INNER JOIN `tbl_resident`
                      ON `pcq_resident_id` = `resident_id`
                      INNER JOIN `tbl_basic_contact`
                      ON `resident_bc_id` = `bc_id`
                      INNER JOIN `tbl_address`
                      ON `resident_address_id` = `address_id`
                      INNER JOIN `tbl_barangay`
                      ON `address_brgy_id` = `brgy_id`
                      LEFT JOIN  `tbl_complaint`
                      ON `resident_id` = `complaint_resident_id`" . $where_str_query;
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result['count'] = $stmt->fetch(\PDO::FETCH_ASSOC)['num'];

    $sql_statement = "SELECT ".$column_str_query."
                      FROM `tbl_pc_request`
                      INNER JOIN `tbl_resident`
                      ON `pcq_resident_id` = `resident_id`
                      INNER JOIN `tbl_basic_contact`
                      ON `resident_bc_id` = `bc_id`
                      INNER JOIN `tbl_address`
                      ON `resident_address_id` = `address_id`
                      INNER JOIN `tbl_barangay`
                      ON `address_brgy_id` = `brgy_id`
                      LEFT JOIN  `tbl_complaint`
                      ON `resident_id` = `complaint_resident_id`" . $where_str_query . " ". $group_str_query . " " . $order_str_query. " ".$limit_str_query;
		$stmt = $this->prepare($sql_statement);
    $params[':limit'] = $limit;
    $params[':offset'] = $offset;

		$stmt->execute($params);
		$result['data'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);


    $result['total_count'] = $this->getAllCount();

		return $result;
  }


  public function checkExistByResident($pcq_resident_id){
    $params = array(
      ':pcq_resident_id'=>$pcq_resident_id
    );
    $sql_statement = "SELECT *
                      FROM `tbl_pc_request`
                      WHERE `pcq_status` IN('0', '1') AND `pcq_resident_id` = :pcq_resident_id ";

		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  public function getTotalPendingByDate($date){
    $params = array(
      ':date_filter'  => date('Y-m-d', strtotime($date))
    );
    $sql_statement = "SELECT pcq_status
                      FROM `tbl_pc_request`
                      WHERE DATE(pcq_request_date) = :date_filter";

		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    $summarize = array(
      'processed' => 0
    , 'total'     => 0
    , 'percentage'=> 0
    );
    foreach($result as $entry){
      if($entry['pcq_status'] == '2' || $entry['pcq_status'] == '3'){
          $summarize['processed'] += 1;
      }
    }
    $summarize['total'] = count($result);
    if($summarize['total'] > 0){
      $summarize['percentage'] = ($summarize['processed'] / $summarize['total']) * 100;
    }

    return $summarize;
  }

  public function generate_pcq_no($id){
    while(strlen($id)<8){
      $id = '0'.$id;
    }
    return 'Q-'.$id;
  }


}
