<?php
namespace App\Mapper;
use Sys\Mapper\Mapper;

class ComplaintMapper extends Mapper{

  protected $_table = 'tbl_complaint';

  public function selectDataTable($filter, $columns, $limit, $offset, $order, $add_filter = array()){
    $result = array(
      'data'  => array()
    , 'total_count'=>0
    , 'count'=>0
    );

    $order_str_query = "ORDER BY ";
    $limit_str_query = "LIMIT :limit OFFSET :offset";
    $column_str_query = "";
    $where_str_query = "";
    $params = array();

    $column_str_query = ' CONCAT(bc_last_name, \', \', bc_first_name, \' \', bc_middle_name, \' \', bc_name_ext) as resident,
                        bc_gender, TIMESTAMPDIFF(YEAR, resident_birthdate, CURDATE()) AS age,
                        complaint_title, brgy_name, complaint_date_filed, complaint_type, complaint_id';
    if(!empty($filter)){
        $where_str_query .= "  (complaint_is_resolved = 0  AND (bc_first_name LIKE :bc_first_name OR bc_middle_name LIKE :bc_middle_name
                            OR bc_last_name LIKE :bc_last_name OR bc_name_ext LIKE :bc_name_ext OR
                            brgy_name LIKE :brgy_name OR bc_gender LIKE :bc_gender OR TIMESTAMPDIFF(YEAR, resident_birthdate, CURDATE()) LIKE :age
                            OR complaint_title LIKE :complaint_title OR complaint_date_filed LIKE :complaint_date_filed
                            OR complaint_type LIKE :complaint_type)) ";
        $params[':bc_first_name'] = '%'.$filter.'%';
        $params[':bc_middle_name'] = '%'.$filter.'%';
        $params[':bc_last_name'] = '%'.$filter.'%';
        $params[':bc_name_ext'] = '%'.$filter.'%';
        $params[':brgy_name'] = '%'.$filter.'%';
        $params[':bc_gender'] = '%'.$filter.'%';
        $params[':age'] = '%'.$filter.'%';
        $params[':complaint_title'] = '%'.$filter.'%';
        $params[':complaint_date_filed'] = '%'.$filter.'%';
        $params[':complaint_type'] = '%'.$filter.'%';
    }

    if(!empty($add_filter)){
      if($add_filter['brgy'] != '0'){
        if(!empty($where_str_query)){
          $where_str_query .= ' AND ';
        }
        $where_str_query .= " brgy_id = '".$add_filter['brgy']."' ";
      }
    }

    foreach($order as $i=>$_order){
      $order_str_query .= $_order['col']." ".$_order['type'];
      if(next($order)){
        $order_str_query .= ", ";
      }
    }

    if(strlen($where_str_query)>0){
      $where_str_query = ' WHERE '.$where_str_query;
    }

    $sql_statement = "SELECT COUNT(1) as 'num'
                      FROM `tbl_complaint`
                      INNER JOIN `tbl_resident`
                      ON `complaint_resident_id` = `resident_id`
                      INNER JOIN `tbl_basic_contact`
                      ON `resident_bc_id` = `bc_id`
                      INNER JOIN `tbl_address`
                      ON `resident_address_id` = `address_id`
                      INNER JOIN `tbl_barangay`
                      ON `address_brgy_id` = `brgy_id` " . $where_str_query;
		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result['count'] = $stmt->fetch(\PDO::FETCH_ASSOC)['num'];

    $sql_statement = "SELECT ".$column_str_query."
                        FROM `tbl_complaint`
                        INNER JOIN `tbl_resident`
                        ON `complaint_resident_id` = `resident_id`
                        INNER JOIN `tbl_basic_contact`
                        ON `resident_bc_id` = `bc_id`
                        INNER JOIN `tbl_address`
                        ON `resident_address_id` = `address_id`
                        INNER JOIN `tbl_barangay`
                        ON `address_brgy_id` = `brgy_id` " . $where_str_query . " " . $order_str_query. " ".$limit_str_query;
		$stmt = $this->prepare($sql_statement);
    $params[':limit'] = $limit;
    $params[':offset'] = $offset;

		$stmt->execute($params);
		$result['data'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    $result['total_count'] = $this->getAllCount();

		return $result;
  }

  public function selectComplaintByResident($resident_id){
    $params = array(
      ':complaint_resident_id'=>$resident_id
    );
    $sql_statement = "SELECT *
                      FROM `tbl_complaint`
                      WHERE `complaint_resident_id` = :complaint_resident_id
                      ORDER BY complaint_date_filed DESC ";

		$stmt = $this->prepare($sql_statement);
		$stmt->execute($params);
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

}
