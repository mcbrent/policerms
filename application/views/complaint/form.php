<div class="wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-10">
      <h5><?php echo ucfirst($action) ?> Complaint</h5>
    </div>

    <div class="col-lg-2 text-right">
      <button class="btn btn-info dim has-tooltip form-submit" name="save" title="Save Complaint" data-form="main-form"><i class="fa fa-<?php if($action == 'add'){ echo "file"; } else if($action == 'edit'){ echo "pencil"; } ?>"></i> <?php echo $action ?> COMPLAINT</button>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-danger">
        <div class="panel-heading">
          <h3><?php if($action == 'add'){ echo "Creating"; } else if($action == 'edit'){ echo "Updating"; }?> complaint</h3>
        </div>
        <div class="panel-body">
          <blockquote>
              <small>Complaint against <strong id="resident-fullname">
                <?php echo strtoupper(
                  $form_data['resident_first_name']. ' '.
                  $form_data['resident_middle_name']. ' '.
                  $form_data['resident_last_name']. ' '.
                  $form_data['resident_name_ext'])?></strong>,
                  <strong id="resident-age">
                  <?php echo date_diff(date_create($form_data['resident_birthday']), date_create('now'))->y?>
                  </strong> year/s old, <strong class="text-uppercase"><?php echo $form_data['resident_gender']?></strong>,
                  from <strong class="text-uppercase" id="resident-address">
                  <?php echo $form_data['add_desc']. " - ".$form_data['address_brgy']['brgy_name'] ?>, Batangas City, Philippines</strong>.
              </small>
          </blockquote>
          <div class="hr-line-solid"></div>

          <form id="main-form" class="form-horizontal" method="POST" enctype="multipart/form-data" action="?">
            <div class="form-group">
              <div class="col-sm-12">
                <h4>Complaint Title</h4>
                <input class="form-control text-uppercase" type="text" name="complaint-title" placeholder="Complaint Title" value="<?php echo $form_data['complaint_title'] ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <h4>Complaint Date</h4>
                <input type="text" name="complaint-date" class="form-control datepicker" placeholder="Select Complaint Date" value="<?php echo $form_data['complaint_date_filed'] ?>">
              </div>
              <div class="col-sm-6">
                <h4>Complaint Type</h4>
                <select class="form-control select2-basic" name="complaint-type" data-placeholder="Select Complaint Type">
                    <option></option>
                    <option value="MILD" <?php echo $form_data['complaint_type'] == 'MILD'? 'selected' : '' ?>>MILD</option>
                    <option value="MODERATE" <?php echo $form_data['complaint_type'] == 'MODERATE'? 'selected' : '' ?>>MODERATE</option>
                    <option value="SEVERE" <?php echo $form_data['complaint_type'] == 'SEVERE'? 'selected' : '' ?>>SEVERE</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <textarea name="complaint-content" class="summernote">
                  <?php echo $form_data['complaint_description'] ?>
                </textarea>
              </div>
            </div>
          </form>
        </div>
      </div>


    </div>
  </div>


</div>
<script src="<?php echo JS_DIR ?>components/resident/resident_profile.js"></script>
