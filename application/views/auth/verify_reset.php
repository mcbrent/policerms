<div class="middle-box text-center">
    <div class="panel panel-info">
        <div class="panel-heading">
            Change Password
        </div>
        <div class="panel-body">
            <?php if($is_valid){ ?>
            <form class="m-t" role="form" method="POST" action="">
                <div class="form-group">
                    <input type="password" name="user_password" class="form-control" placeholder="Enter new Password" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="re_user_password" class="form-control" placeholder="Confirm new Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Save Password</button>
            </form>
             <?php }
             else{ ?>

             <h3 class="text-danger">Invalid URL</h3>
             <?php }?>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $("form").validate({
      rules :{
        'user_password':{
          required : true,
          username : true,
          minlength: 6,
          maxlength: 30
        },
        're_user_password':{
          required : true,
          equalTo : "[name=user_password]"
        }

      }

    });
});
</script>
