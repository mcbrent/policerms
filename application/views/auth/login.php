<div class="middle-box text-center">
    <div>
        <div>

            <h1 class="logo"><img class="responsive" src="<?php echo IMG_DIR?>Batangas_City_Seal.png"></h1>

        </div>

        <form class="m-t" role="form" method="POST" action="<?php echo $action_url ?>">
            <div class="form-group">
                <input type="text" name="user_email" class="form-control" placeholder="Email / Username" required="">
            </div>
            <div class="form-group">
                <input type="password" name="user_password" class="form-control" placeholder="Password" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            <a href="<?php echo DOMAIN?>auth/reset-password" class="btn btn-danger block full-width m-b">Reset Password</a>
        </form>
    </div>
</div>
