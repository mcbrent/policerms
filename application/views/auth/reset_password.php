<div class="middle-box text-center">
    <div class="panel panel-info">
        <div class="panel-heading">Reset Password</div>
        <div class="panel-body">
            <?php if(!isset($has_sent)){ ?>
            <form class="m-t" role="form" method="POST" action="">
                <div class="form-group">
                    <input type="email" name="user_email" class="form-control" placeholder="Email" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Send Verification</button>
            </form>
        <?php } else{ ?>
            <p>Check your email <b><?php echo $email ?></b> for the reset password link</p>
        <?php }?>
        </div>
    </div>
</div>
<script type="application/javascript">
$(document).ready(function(){
    $("form").validate({
      rules :{
        'user_email': {
          required : true,
          email : true
        }
      }
    });
});
</script>
