<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Police Clearance Request List</h5>
                  </div>
                  <div class="ibox-content">

                    <div class="table-responsive">
                      <table id="police-clearance-request-list" class="table table-striped table-bordered table-hover datatable-basic" data-config="{}">
                        <thead>
                          <tr>
                            <th class="col-md-1 text-center">Process No</th>
                            <th class="col-md-3 text-center">Name</th>
                            <th class="col-md-2 text-center">Purpose</th>
                            <th class="col-md-1 text-center">Barangay</th>
                            <th class="col-md-2 text-center">Other Info</th>
                            <th class="col-md-1 text-center">No. Of Active Complaint</th>
                            <th class="col-md-1 text-center">Status</th>
                            <th class="col-md-1 text-center no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>

                  </div>
              </div>
          </div>
    </div>

</div>
<script src="<?php echo JS_DIR ?>components/clearance/request_list.js"></script>
