<div class="wrapper wrapper-content">
    <div class="container">

      <div class="row">
        <div class="col-lg-12 gray-bg text-dark" >
          <form id="main-form" action="" method="POST">
            <div class="well">
              <p>Requirements for Police Clearance:
                <ul>
                  <li>Barangay Clearance</li>
                  <li>Court Clearance (for any one involved/misidentify in a crime/legal case)</li>
                </ul>
              </p>
              <p class="text-danger">Note : All fields with asterisk( * ) are required.</p>
            </div>
            <div id="main-form" class="form-horizontal">
              <h5>First Name, Middle Name, Last Name, Name Extension<span class="text-danger">*</span></h5>
              <div class="form-group">
                  <div class="col-sm-3"><input name="resident-first-name" type="text" class="form-control text-uppercase" placeholder="First Name"></div>
                  <div class="col-sm-3"><input name="resident-middle-name" type="text" class="form-control text-uppercase" placeholder="Middle Name"></div>
                  <div class="col-sm-3"><input name="resident-last-name" type="text" class="form-control text-uppercase" placeholder="Last Name"></div>
                  <div class="col-sm-3"><input name="resident-name-ext" type="text" class="form-control text-uppercase" placeholder="Name Extension"></div>
              </div>
              <div class="hr-line-dashed"></div>
              <h5>Current Address<span class="text-danger">*</span></h5>
              <div class="form-group">
                  <div class="col-sm-12"><input type="text" name="add-desc" class="form-control text-uppercase" placeholder="(House/Unit No., Floor & Bldg./Street, Lot / Blk, Brgy / Village)"></div>
                  <div class="col-sm-3 m-t-sm">
                    <input type="text" readonly class="form-control text-uppercase" value="Batangas"></input>
                  </div>
                  <div class="col-sm-3 m-t-sm">
                    <input type="text" readonly class="form-control text-uppercase" value="Batangas City"></input>
                  </div>
                  <div class="col-sm-6 m-t-sm">
                    <select class="form-control text-uppercase" name="current-barangay" data-placeholder="Select Barangay">
                        <option></option>
                    </select>
                  </div>
              </div>
              <div class="hr-line-dashed"></div>
              <div class="form-group">
                <div class="col-sm-3">
                  <h5>Gender<span class="text-danger">*</span></h5>
                  <label class="checkbox-inline i-checks"> <input type="radio" value="MALE" name="resident-gender" checked> <i></i> Male </label>
                  <label class="checkbox-inline i-checks"> <input type="radio" value="FEMALE" name="resident-gender"> <i></i> Female</label>
                </div>
                <div class="col-sm-3">
                  <h5>Birthday <span id="resident-age"></span><span class="text-danger">*</span></h5>
                  <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" autocomplete="off" placeholder="mm/dd/yyyy" name="resident-birthday" class="form-control datepicker residency-validator" >
                  </div>
                </div>
                <div class="col-sm-3">
                  <h5>Civil Status<span class="text-danger">*</span></h5>
                  <select class="form-control select2-basic" name="resident-civil-status" data-placeholder="Select Civil Status">
                      <option></option>
                      <option value="SINGLE">SINGLE</option>
                      <option value="MARRIED">MARRIED</option>
                      <option value="WIDOWED">WIDOWED</option>
                  </select>
                </div>
                <div class="col-sm-3">
                  <h5>Nationality<span class="text-danger">*</span></h5>
                  <input name="resident-nationality" type="text" class="form-control text-uppercase" placeholder="Enter Nationality">
                </div>
              </div>
              <div class="hr-line-dashed"></div>
              <h4>Contact Details</h4>
              <div class="form-group">
                <div class="col-sm-4">
                  <h5>Mobile Number<span class="text-danger">*</span></h5>
                  <input name="phone-number-1" type="number" class="form-control" maxlength="11" placeholder="Enter Mobile Number (eg. 09161234567)">
                </div>
                <div class="col-sm-4">
                  <h5>Telephone</h5>
                  <input name="phone-number-2" type="number" class="form-control" maxlength="11" placeholder="Enter Telephone Number (eg. 0437231234)">
                </div>
              </div>
              <div class="hr-line-dashed"></div>
              <h4>Important Details</h4>
              <div class="form-group">
                <div class="col-sm-4">
                  <h5>Nickname/Alias<span class="text-danger">*</span></h5>
                  <input name="resident-alias" type="text" class="form-control text-uppercase" placeholder="Enter Nickname/Alias">
                </div>
                <div class="col-sm-4">
                  <h4>Occupation<span class="text-danger">*</span></h4>
                  <input name="resident-occupation" type="text" class="form-control text-uppercase" placeholder="Enter Occupation">
                </div>
                <div class="col-sm-4">
                  <h4>Religion<span class="text-danger">*</span></h4>
                  <input name="resident-religion" type="text" class="form-control text-uppercase" placeholder="Enter Religion">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3">
                  <h4>Years in the community<span class="text-danger">*</span></h4>
                  <input name="resident-yrs-community" type="number" min="0" max="100" class="form-control residency-validator" value="0" placeholder="Enter Years in the community">
                </div>
                <div class="col-sm-3">
                  <h4>Weight (kg)<span class="text-danger">*</span></h4>
                  <input name="resident-weight" type="number" class="form-control" min="0" max="200" placeholder="Enter Weight">
                </div>
                <div class="col-sm-3">
                  <h4>Height (cm)<span class="text-danger">*</span></h4>
                  <input name="resident-height" type="number" class="form-control" min="0" max="250" placeholder="Enter Height">
                </div>
                <div class="col-sm-3">
                  <h4>Completion<span class="text-danger">*</span></h4>
                  <select class="form-control select2-basic" name="resident-completion" data-placeholder="Select Completion">
                      <option></option>
                      <option value="fair">fair</option>
                      <option value="light">light</option>
                      <option value="brown">brown</option>
                      <option value="dark">dark</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-4">
                  <h4>Hair color<span class="text-danger">*</span></h4>
                  <select class="form-control select2-basic" name="resident-hair-color" data-placeholder="Select Hair Color">
                      <option></option>
                      <option value="BLACK">BLACK</option>
                      <option value="BALD">BALD</option>
                      <option value="BROWN">BROWN</option>
                      <option value="BLONDE">BLONDE</option>
                      <option value="BLUE">BLUE</option>
                      <option value="BRONZE">BRONZE</option>
                      <option value="DARK BROWN">DARK BROWN</option>
                      <option value="GREEN">GREEN</option>
                      <option value="GRAY">GRAY</option>
                      <option value="RED">RED</option>
                      <option value="WHITE">WHITE</option>
                  </select>
                </div>
                <div class="col-sm-4">
                  <h4>Eye color<span class="text-danger">*</span></h4>
                  <select class="form-control select2-basic" name="resident-eye-color" data-placeholder="Select Eye Color">
                      <option></option>
                      <option value="BLACK">BLACK</option>
                      <option value="BROWN">BROWN</option>
                      <option value="BLUE">BLUE</option>
                      <option value="GREEN">GREEN</option>
                      <option value="GRAY">GRAY</option>
                  </select>
                </div>
                <div class="col-sm-4">
                  <h4>Body Size<span class="text-danger">*</span></h4>
                  <select class="form-control select2-basic" name="resident-body-size" data-placeholder="Select Body Size">
                      <option></option>
                      <option value="Extra extra large">Extra extra large</option>
                      <option value="Extra large">Extra large</option>
                      <option value="Large">Large</option>
                      <option value="Medium">Medium</option>
                      <option value="Small">Small</option>
                      <option value="Extra Small">Extra Small</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                  <h4>Distinguishing Marks<span class="text-danger">*</span></h4>
                  <input name="resident-distinguishing-marks" type="text" class="form-control text-uppercase" placeholder="Enter Distinguishing Marks">
                </div>
              </div>
              <div class="hr-line-dashed"></div>
              <h4>Purpose<span class="text-danger">*</span></h4>
              <div class="form-group">
                <div class="col-sm-12">
                  <select name="purpose" class="form-control select2-basic" data-placeholder="SELECT PURPOSE">
                    <option></option>
                    <option value=""></option>
                    <option value="ADOPTION REQUIREMENT">ADOPTION REQUIREMENT</option>
                    <option value="AIRPORT REQUIREMENT">AIRPORT REQUIREMENT</option>
                    <option value="APPRENTICESHIP  REQUIREMENT">APPRENTICESHIP  REQUIREMENT</option>
                    <option value="ARMED FORCES OF THE PHILIPPINES REQUIREMENT">ARMED FORCES OF THE PHILIPPINES REQUIREMENT</option>
                    <option value="BANK REQUIREMENT">BANK REQUIREMENT</option>
                    <option value="BLASTER REQUIREMENT">BLASTER REQUIREMENT</option>
                    <option value="BJMP REQUIREMENT">BJMP REQUIREMENT</option>
                    <option value="BURIAL REQUIREMENT">BURIAL REQUIREMENT</option>
                    <option value="BUSINESS REQUIREMENT">BUSINESS REQUIREMENT</option>
                    <option value="CHANGE OF BIRTH DATE">CHANGE OF BIRTH DATE</option>
                    <option value="CHANGE OF GENDER FROM FEMALE TO MALE">CHANGE OF GENDER FROM FEMALE TO MALE</option>
                    <option value="CHANGE OF GENDER FROM MALE TO FEMALE">CHANGE OF GENDER FROM MALE TO FEMALE</option>
                    <option value="CHANGE OF MIDDLE NAME">CHANGE OF MIDDLE NAME</option>
                    <option value="CHANGE OF NAME">CHANGE OF NAME</option>
                    <option value="CITOM REQUIREMENT">CITOM REQUIREMENT</option>
                    <option value="CIVIL SERVICE COMMISSION REQUIREMENT">CIVIL SERVICE COMMISSION REQUIREMENT</option>
                    <option value="DMDP REQUIREMENT">DMDP REQUIREMENT</option>
                    <option value="DOH REQUIREMENT">DOH REQUIREMENT</option>
                    <option value="DRIVERS LICENSE REQUIREMENT">DRIVERS LICENSE REQUIREMENT</option>
                    <option value="DSWD REQUIREMENT">DSWD REQUIREMENT</option>
                    <option value="EMPLOYMENT ABROAD REQUIREMENT">EMPLOYMENT ABROAD REQUIREMENT</option>
                    <option value="FIANCEE VISA REQUIREMENT">FIANCEE VISA REQUIREMENT</option>
                    <option value="FIREARM LICENSE REQUIREMENT">FIREARM LICENSE REQUIREMENT</option>
                    <option value="FRANCHISE REQUIREMENT">FRANCHISE REQUIREMENT</option>
                    <option value="GLOBE REQUIREMENT">GLOBE REQUIREMENT</option>
                    <option value="GSIS REQUIREMENT">GSIS REQUIREMENT</option>
                    <option value="HOSPITAL REQUIREMENT">HOSPITAL REQUIREMENT</option>
                    <option value="JPIC HOUSING REQUIREMENT">JPIC HOUSING REQUIREMENT</option>
                    <option value="IMMIGRANT VISA REQUIREMENT">IMMIGRANT VISA REQUIREMENT</option>
                    <option value="LICENSE TO OPERATE SECURITY AGENCY REQUIREMENT">LICENSE TO OPERATE SECURITY AGENCY REQUIREMENT</option>
                    <option value="LICENSE TO OWN AND POSSESS FIREARM REQUIREMENT">LICENSE TO OWN AND POSSESS FIREARM REQUIREMENT</option>
                    <option value="LICENSE TO DEAL FIREARMS AND AMMUNITION REQUIREMENT">LICENSE TO DEAL FIREARMS AND AMMUNITION REQUIREMENT</option>
                    <option value="LICENSE TO PURCHASE EXPLOSIVES REQUIREMENT">LICENSE TO PURCHASE EXPLOSIVES REQUIREMENT</option>
                    <option value="LOAN REQUIREMENT">LOAN REQUIREMENT</option>
                    <option value="LOCAL CIVIL REGISTRAR REQUIREMENT">LOCAL CIVIL REGISTRAR REQUIREMENT</option>
                    <option value="LOCAL EMPLOYMENT REQUIREMENT">LOCAL EMPLOYMENT REQUIREMENT</option>
                    <option value="LTFRB UBER REQUIREMENT">LTFRB UBER REQUIREMENT</option>
                    <option value="LTFRB GRAB CAR REQUIREMENT">LTFRB GRAB CAR REQUIREMENT</option>
                    <option value="LTO DEPUTATION REQUIREMENT">LTO DEPUTATION REQUIREMENT</option>
                    <option value="MASON REQUIREMENT">MASON REQUIREMENT</option>
                    <option value="MARRIAGE REQUIREMENT">MARRIAGE REQUIREMENT</option>
                    <option value="M. LHUILLIER REQUIREMENT">M. LHUILLIER REQUIREMENT</option>
                    <option value="MISSIONARY REQUIREMENT">MISSIONARY REQUIREMENT</option>
                    <option value="NATURALIZATION REQUIREMENT">NATURALIZATION REQUIREMENT</option>
                    <option value="NON-IMMIGRANT VISA">NON-IMMIGRANT VISA</option>
                    <option value="ON THE JOB TRAINING REQUIREMENT">ON THE JOB TRAINING REQUIREMENT</option>
                    <option value="PAG-IBIG REQUIREMENT">PAG-IBIG REQUIREMENT</option>
                    <option value="PARTNERSHIP VISA">PARTNERSHIP VISA</option>
                    <option value="PASSPORT REQUIREMENT">PASSPORT REQUIREMENT</option>
                    <option value="PDEA REQUIREMENT">PDEA REQUIREMENT</option>
                    <option value="PERMANENT RESIDENCY VISA REQUIREMENT">PERMANENT RESIDENCY VISA REQUIREMENT</option>
                    <option value="PHILHEALTH REQUIREMENT">PHILHEALTH REQUIREMENT</option>
                    <option value="PHILIPPINE MILITARY ACADEMY REQUIREMENT">PHILIPPINE MILITARY ACADEMY REQUIREMENT</option>
                    <option value="PHILIPPINE RETIREMENT AUTHORITY REQUIREMENT">PHILIPPINE RETIREMENT AUTHORITY REQUIREMENT</option>
                    <option value="PHILIPPINE STATISTICS AUTHORITY REQUIREMENT">PHILIPPINE STATISTICS AUTHORITY REQUIREMENT</option>
                    <option value="PLDT REQUIREMENT">PLDT REQUIREMENT</option>
                    <option value="PNP REQUIREMENT">PNP REQUIREMENT</option>
                    <option value="PNPA REQUIREMENT">PNPA REQUIREMENT</option>
                    <option value="POSTAL I.D. REQUIREMENT">POSTAL I.D. REQUIREMENT</option>
                    <option value="PRC REQUIREMENT">PRC REQUIREMENT</option>
                    <option value="PROBATION REQUIREMENT">PROBATION REQUIREMENT</option>
                    <option value="R2 REQUIREMENT">R2 REQUIREMENT</option>
                    <option value="RCBC REQUIREMENT">RCBC REQUIREMENT</option>
                    <option value="SCHOOL REQUIREMENT">SCHOOL REQUIREMENT</option>
                    <option value="SEAMANS VISA">SEAMANS VISA</option>
                    <option value="SECURITY GUARD LICENSE REQUIREMENT">SECURITY GUARD LICENSE REQUIREMENT</option>
                    <option value="SECURITY OFFICER REQUIREMENT">SECURITY OFFICER REQUIREMENT</option>
                    <option value="SOCIAL SECURITY SYSTEM REQUIREMENT">SOCIAL SECURITY SYSTEM REQUIREMENT</option>
                    <option value="SEAMANS BOOK REQUIREMENT">SEAMANS BOOK REQUIREMENT</option>
                    <option value="PANTAWID PAMILYANG PILIPINO PROGRAM REQUIREMENT">PANTAWID PAMILYANG PILIPINO PROGRAM REQUIREMENT</option>
                    <option value="PERMIT TO CARRY FIREARMS OUTSIDE RESIDENCE">PERMIT TO CARRY FIREARMS OUTSIDE RESIDENCE</option>
                    <option value="STUDENT VISA REQUIREMENT">STUDENT VISA REQUIREMENT</option>
                    <option value="TESDA REQUIREMENT">TESDA REQUIREMENT</option>
                    <option value="TOURIST VISA REQUIREMENT">TOURIST VISA REQUIREMENT</option>
                    <option value="TOUR GUIDE LICENSE REQUIREMENT">TOUR GUIDE LICENSE REQUIREMENT</option>
                    <option value="WORKING ABROAD REQUIREMENT">WORKING ABROAD REQUIREMENT</option>
                    <option value="WORKING VISA REQUIREMENT">WORKING VISA REQUIREMENT</option>
                    <option value="WESTERN UNION REQUIREMENT">WESTERN UNION REQUIREMENT</option>
                    <option value="ALL OTHER PURPOSE NOT HERE MENTIONED">ALL OTHER PURPOSE NOT HERE MENTIONED</option>
                  </select>
                </div>
              </div>
              <h4>Target Date to visit Police Station for Biometrics, Payment and Release of Police Clearance<span class="text-danger">*</span></h4>
              <div class="form-group">
                <div class="col-sm-6">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" placeholder="mm/dd/yyyy" type="text" name="target-date" class="form-control datepicker" >
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <select name="target-time" class="form-control select2-basic" data-placeholder="SELECT TIME">
                      <option value="8:00">8:00</option>
                      <option value="9:00">9:00</option>
                      <option value="10:00">10:00</option>
                      <option value="11:00">11:00</option>
                      <option value="12:00">12:00</option>
                      <option value="13:00">13:00</option>
                      <option value="14:00">14:00</option>
                      <option value="15:00">15:00</option>
                      <option value="16:00">16:00</option>
                      <option value="17:00">17:00</option>
                      <option value="18:00">18:00</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="g-recaptcha" data-sitekey="6LctkHAUAAAAANlQfrFT_ZGdOYuNCEOjoRPCa0aH" data-callback="enable" data-expired-callback="disable"></div>
              <div class="hr-line-solid"></div>
            </div>

            <button id="request-police-clearance" type="submit" class="btn btn-danger btn-block btn-outline dim" disabled><i class="fa fa-paper-plane"></i> Request Police Clearance</button>

            </form>
        </div>
      </div>

    </div>

</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
function enable(){
  document.getElementById("request-police-clearance").disabled = false;
}

function disable(){
  document.getElementById("request-police-clearance").disabled = true;
}
</script>
<script src="<?php echo JS_DIR ?>components/clearance/request.js"></script>
