<div class="wrapper wrapper-content ">
  <div class="row">

    <div class="col-lg-10">

    </div>

    <div class="col-lg-2 text-right">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-info container">
        <div class="panel-heading">
          <i class="fa fa-address-card"></i>
          Processing Police Clearance
        </div>
        <div class="panel-body form-horizontal">
          <form method="POST" id="process-police" enctype="multipart/form-data" >
            <div class="form-group">
              <div class="col-md-12">
                <h5>Control No.</h5>
                <input type="text" class="form-control" name="pc-control-no" placeholder="Enter Control No." value=""/>
              </div>
              <div class="col-md-12">
                <h5>OR No.</h5>
                <input type="text" class="form-control" name="pc-or-no" placeholder="Enter OR No." value=""/>
              </div>
              <div class="col-md-12">
                <h5>CTC No.</h5>
                <input type="text" class="form-control" name="pc-ctc-no" placeholder="Enter CTC No." value=""/>
              </div>
              <div class="col-md-12">
                <h5>Date Issued</h5>
                <input type="text" class="form-control datepicker" name="pc-date-issued" autocomplete="off" placeholder="Enter Date Issued" value="<?php echo date('m/d/Y')?>"/>
              </div>
              <div class="col-md-12">
                <h5>Place Issued</h5>
                <input type="text" class="form-control" name="pc-place-issued" placeholder="Enter Place Issued" value="City of Batangas, Batangas 4232"/>
              </div>
              <div class="col-md-12">
                <div class="hr-line-dashed"></div>
              </div>
              <div class="col-md-12">
                <h5>Name</h5>
                <input type="text" class="form-control" name="pc-name" value="<?php echo $form_data['resident_last_name'].', '.$form_data['resident_first_name'].' '.$form_data['resident_middle_name'] ?>"/>
              </div>
              <div class="col-md-12">
                <h5>Alias</h5>
                <input type="text" class="form-control" name="pc-alias" value="<?php echo $form_data['resident_nickname'] ?>"/>
              </div>
              <div class="col-md-12">
                <h5>Address</h5>
                <input type="text" class="form-control" name="pc-address" value="<?php echo $form_data['add_desc'].' Batangas City' ?>"/>
              </div>
              <div class="col-md-12">
                <h5>Birthday</h5>
                <input type="text" class="form-control" name="pc-birthday" value="<?php echo date('F d, Y', strtotime($form_data['resident_birthday'])) ?>"/>
              </div>
              <div class="col-md-12">
                <h5>Place of Birth</h5>
                <input type="text" class="form-control" name="pc-place-birth" value="Batangas City"/>
              </div>
              <div class="col-md-12">
                <h5>Gender</h5>
                <input type="text" class="form-control" name="pc-gender" value="<?php echo strtoupper($form_data['resident_gender']) ?>"/>
              </div>
              <div class="col-md-12">
                <h5>Civil Status</h5>
                <input type="text" class="form-control" name="pc-civil-status" value="<?php echo strtoupper($form_data['resident_civil_status']) ?>"/>
              </div>
              <div class="col-md-12">
                <h5>Citizenship</h5>
                <input type="text" class="form-control" name="pc-citizenship" value="<?php echo strtoupper($form_data['resident_nationality']) ?>"/>
              </div>
              <div class="col-md-12">
                <h5>Remarks</h5>
                <input type="text" class="form-control" name="pc-remarks" value="<?php echo $form_data['remarks']?>"/>
              </div>
              <div class="col-md-12">
                <h5>Purpose</h5>
                <input type="text" class="form-control" name="pc-purpose" value="<?php echo strtoupper($form_data['purpose']) ?>"/>
              </div>
              <div class="col-md-12">
                <h5>Valid Until</h5>
                <input type="text" class="form-control" name="pc-valid" value="<?php echo date('F d, Y', strtotime("+3 months", strtotime(date('Y-m-d'))));?>"/>
              </div>
              <div class="col-md-12">
                <h5>Image</h5>
                <input type="file" name="pc-img" accept="image/*">
                <br>
              </div>
              <div class="col-md-12">
                <input id="upload-process" class="btn btn-info btn-block btn-outline" type="submit" value="Process"/>
              </div>
            </div>
          </form>

        </div>
      </div>


      </div>
    </div>
  </div>


</div>
<script type="application/javascript">
$(document).ready(function(){


  // $("#process-police").validate({
  //   rules : {
  //       'pc-img' : {
  //         required : true,
  //         accept: "image/jpeg, image/pjpeg, image/jpg, image/png, image/bmp"
  //       }
  //   }
  //  });


  $('#process-police').on('submit', function(event){
    event.preventDefault();
    if($("#process-police").valid()){
      $.ajax({
        url: global.site_name + 'clearance/format_process',
        method:'POST',
        data: new FormData(this),
        dataType:'JSON',
        cache:false,
        processData:false,
        contentType:false,
        success:function(result){
          var win = window.open(global.site_name + 'upload/pdf/'+result.pdf_name, '_blank');
         if (win) {
             //Browser has allowed it to be opened
             win.focus();
         } else {
             //Browser has blocked it
             alert('Please allow popups for this website');
         }
          // $('#status-alert').show();
          // $('#status-alert #status-alert-message').text(data.message);
          // $('#status-alert').removeClass();
          // $('#status-alert').addClass('alert ' + data.class_name)
        },
        error:function(){
          bootbox.alert("Some fields are empty")
        }
      });
    }

  })

});
</script>
