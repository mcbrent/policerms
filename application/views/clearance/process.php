<div class="wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-10">

    </div>

    <div class="col-lg-2 text-right">
      <button id="print-format" class="btn btn-warning dim has-tooltip" value="<?php echo $form_data['pcq_id'] ?>" title="Generate Police Clearance"><i class="fa fa-cog"></i></button>
      <button id="print-process" class="btn btn-info dim has-tooltip" value="<?php echo $form_data['resident_id'] ?>" title="Print Guide"><i class="fa fa-print"></i></button>
      <button class="btn btn-success dim has-tooltip save" value="mark-finish" title="Mark as Finish"><i class="fa fa-check"></i></button>
      <button class="btn btn-danger dim has-tooltip save" value="reject" title="Reject"><i class="fa fa-times"></i></button>
      <input type="hidden" id="police-clearance-id" value="<?php echo $form_data['pcq_id'] ?>">
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-info">
        <div class="panel-heading">
          <i class="fa fa-address-card"></i>
          Basic Information
        </div>
        <div class="panel-body form-horizontal">
          <h5>First Name, Middle Name, Last Name, Name Extension</h5>
          <div class="form-group">
              <div class="col-sm-3"><input name="resident-first-name" type="text" class="form-control" placeholder="First Name" readonly value="<?php echo $form_data['resident_first_name'] ?>"></div>
              <div class="col-sm-3"><input name="resident-middle-name" type="text" class="form-control" placeholder="Middle Name" readonly value="<?php echo $form_data['resident_middle_name'] ?>"></div>
              <div class="col-sm-3"><input name="resident-last-name" type="text" class="form-control" placeholder="Last Name" readonly value="<?php echo $form_data['resident_last_name'] ?>"></div>
              <div class="col-sm-3"><input name="resident-name-ext" type="text" class="form-control" placeholder="Name Extension" readonly value="<?php echo $form_data['resident_name_ext'] ?>"></div>
          </div>
          <div class="hr-line-dashed"></div>
          <h5>Current Address</h5>
          <div class="form-group">
              <div class="col-sm-12"><input type="text" name="add-desc" class="form-control" placeholder="(House/Unit No., Floor & Bldg./Street, Lot / Blk, Brgy / Village)" readonly value="<?php echo $form_data['add_desc'] ?>"></div>
              <div class="col-sm-3 m-t-sm">
                <input type="text" readonly class="form-control" value="Batangas"></input>
              </div>
              <div class="col-sm-3 m-t-sm">
                <input type="text" readonly class="form-control" value="Batangas City"></input>
              </div>
              <div class="col-sm-6 m-t-sm">
                <input type="text" readonly class="form-control" value="<?php echo $form_data['address_brgy']['brgy_name'] ?>"></input>
              </div>
          </div>
          <div class="hr-line-dashed"></div>
          <div class="form-group">
            <div class="col-sm-3">
              <h5>Gender</h5>
              <input type="text" readonly class="form-control" value="<?php echo $form_data['resident_gender'] ?>"></input>
            </div>
            <div class="col-sm-3">
              <h5>Birthday (<span id="resident-age"><?php echo date_diff(date_create($form_data['resident_birthday']), date_create('now'))->y ?> years old</span>)</h5>
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" readonly value="<?php echo $form_data['resident_birthday'] ?>">
              </div>
            </div>
            <div class="col-sm-3">
              <h5>Civil Status</h5>
              <input type="text" readonly class="form-control" value="<?php echo $form_data['resident_civil_status'] ?>"></input>
            </div>
            <div class="col-sm-3">
              <h5>Nationality</h5>
              <input readonly name="resident-nationality" type="text" class="form-control" placeholder="Enter Nationality" value="<?php echo $form_data['resident_nationality'] ?>">
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          <h4>Contact Details</h4>
          <div class="form-group">
            <div class="col-sm-4">
              <h5>Contact Number 1</h5>
              <input name="phone-number-1" readonly type="text" class="form-control" placeholder="Enter Main Contact Number" value="<?php echo $form_data['phone_number_1'] ?>">
            </div>
            <div class="col-sm-4">
              <h5>Contact Number 2</h5>
              <input name="phone-number-2" readonly type="text" class="form-control" placeholder="Enter Mobile Number" value="<?php echo $form_data['phone_number_2'] ?>">
            </div>
          </div>
          <div class="hr-line-dashed"></div>
          <h3>Important Details</h3>
          <div class="form-group">
            <div class="col-sm-4">
              <h4>Nickname/Alias</h4>
              <input name="resident-alias" readonly type="text" class="form-control" placeholder="Enter Nickname/Alias" value="<?php echo $form_data['resident_nickname']?>">
            </div>
            <div class="col-sm-4">
              <h4>Occupation</h4>
              <input name="resident-occupation" readonly type="text" class="form-control" placeholder="Enter Occupation" value="<?php echo $form_data['resident_occupation']?>">
            </div>
            <div class="col-sm-4">
              <h4>Religion</h4>
              <input name="resident-religion" readonly type="text" class="form-control" placeholder="Enter Religion" value="<?php echo $form_data['resident_religion']?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-3">
              <h4>Years in the community</h4>
              <input name="resident-yrs-community" readonly type="number" min="0" max="100" class="form-control" placeholder="Enter Years in the community" value="<?php echo $form_data['resident_yrs_community']?>">
            </div>
            <div class="col-sm-3">
              <h4>Weight</h4>
              <input name="resident-weight" readonly type="text" class="form-control" min="0" placeholder="Enter Weight" value="<?php echo $form_data['resident_weight']?>">
            </div>
            <div class="col-sm-3">
              <h4>Height</h4>
              <input name="resident-height" readonly type="text" class="form-control" min="0" placeholder="Enter Height" value="<?php echo $form_data['resident_height']?>">
            </div>
            <div class="col-sm-3">
              <h4>Completion</h4>
              <input readonly type="text" class="form-control" min="0" placeholder="Enter Height" value="<?php echo $form_data['resident_completion'] ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-4">
              <h4>Hair color</h4>
              <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_hair_color'] ?>">
            </div>
            <div class="col-sm-4">
              <h4>Eye color</h4>
              <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_eye_color'] ?>">
            </div>
            <div class="col-sm-4">
              <h4>Body Size</h4>
              <input readonly type="text" class="form-control" value="<?php echo $form_data['resident_body_size'] ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <h4>Distinguishing Marks</h4>
              <input readonly name="resident-distinguishing-marks" type="text" class="form-control" placeholder="Enter Distinguishing Marks" value="<?php echo $form_data['resident_distinguishing_marks']?>">
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-info">
        <div class="panel-heading">
          <i class="fa fa-archive"></i>
          Additional Info
        </div>
        <div class="panel-body form-horizontal">
          <h4>Additional Info</h4>
          <div class="form-group">
            <div class="col-sm-12">
              <?php echo $form_data['resident_add_info'] ?>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-info">
        <div class="panel-heading">
          <i class="fa fa-newspaper-o"></i>
          Report
        </div>
        <div class="panel-body">

        </div>
      </div>

      <div class="panel panel-info">
        <div class="panel-heading">
          <i class="fa fa-cloud"></i>
          Attachment
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table id="attachment-table" class="table table-striped table-bordered table-hover" data-config="{}">
              <thead>
                <tr>
                  <th class="col-md-6 text-center">File Name</th>
                  <th class="col-md-4 text-center">Date</th>
                  <th class="col-md-2 text-center no-sort">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach($form_data['resident_attachment'] as $attachment){ ?>
                <tr>
                  <td class="col-md-6"><?php echo $attachment['rattachment_name']?></td>
                  <td class="col-md-4"><?php echo date('M d Y', strtotime($attachment['rattachment_date']))?></td>
                  <td class="col-md-2 text-center">
                      <button type="button" class="btn blue-bg has-tooltip save-file" title="Download" value="<?php echo $attachment['rattachment_id'] ?>"><i class="fa fa-download"></i></button>
                  </td>
                </tr>
                <?php
                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      </div>
    </div>
  </div>


</div>
<script src="<?php echo JS_DIR ?>components/clearance/process.js"></script>
