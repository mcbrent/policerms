<div class="wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-10">

    </div>

    <div class="col-lg-2 text-right">
      <button class="btn btn-info dim has-tooltip form-submit" name="save" title="Add Barangay Clearance Issuance" data-form="main-form"><i class="fa fa-id-badge"></i> <?php echo $action ?></button>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-info">
        <div class="panel-heading">
          <h5><?php echo ucfirst($action) ?> Barangay Clearance Issuance</h5>
        </div>
        <div class="panel-body">
          <form id="main-form" class="form-horizontal" method="POST" enctype="multipart/form-data" action="?">
            <div class="form-group">
              <div class="col-md-12">
                <h5>Resident Name</h5>
              </div>
              <div class="col-md-11">
                <select class="form-control" name="resident-id" data-placeholder="Select Resident">
                    <option></option>
                </select>
              </div>
              <div class="col-md-1">
                <button type="button" id="view-resident" class="btn btn-info"><i class="fa fa-search"></i></button>
              </div>
            </div>
            <div class="col-md-12"><h4>Barangay Clearance Validity</h4></div>
            <div class="col-md-6">
              <h5>Valid From</h5>
              <input name="valid-from" autocomplete="false" type="text" placeholder="mm/dd/yyyy" class="datepicker form-control" value="<?php echo date('m/d/Y')?>">
            </div>
            <div class="col-md-6">
              <h5>Valid To</h5>
              <input name="valid-to" autocomplete="false" type="text" placeholder="mm/dd/yyyy" class="datepicker form-control" value="<?php echo date('m/d/Y', strtotime("+6 months", strtotime(date('Y-m-d H:i:s'))));?>">
            </div>
            <!-- <div class="col-md-2">
              <h5>&nbsp;</h5>
              <button type="button" id="compute-date" class="btn btn-info btn-block"><i class="fa fa-calendar-o"></i> Compute 6 months </button>
            </div> -->
          </form>
        </div>
      </div>


    </div>
  </div>


</div>
<script src="<?php echo JS_DIR ?>components/brgyclearance/form.js"></script>
