<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>School List</h5>
                      <div class="ibox-tools">
                          <a class="collapse-link">
                              <i class="fa fa-chevron-up"></i>
                          </a>
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                              <i class="fa fa-wrench"></i>
                          </a>
                          <ul class="dropdown-menu dropdown-user">
                              <li><a href="#">Config option 1</a>
                              </li>
                              <li><a href="#">Config option 2</a>
                              </li>
                          </ul>
                          <a class="close-link">
                              <i class="fa fa-times"></i>
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content">

                    <div class="table-responsive">
                      <table class="table table-striped table-bordered table-hover datatable-basic" data-config="{}">
                        <thead>
                          <tr>
                              <th class="col-md-3 text-center">School</th>
                              <th class="col-md-3 text-center"></th>
                              <th class="col-md-3 text-center"></th>
                              <th class="col-md-3 text-center"></th>
                              <th class="col-md-8 text-center">Address</th>
                              <th class="col-md-1 text-center no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>

                  </div>
              </div>
          </div>
    </div>

</div>

<script src="<?php echo JS_DIR ?>components/reference/school_list.js"></script>
