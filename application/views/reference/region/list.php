<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Region List</h5>
                      <div class="ibox-tools">
                          <a class="collapse-link">
                              <i class="fa fa-chevron-up"></i>
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content">

                    <div class="table-responsive">
                      <table class="table table-striped table-bordered table-hover datatable-basic" data-config="{}">
                        <thead>
                          <tr>
                              <th class="col-md-5 text-center">Code</th>
                              <th class="col-md-6 text-center">Region</th>
                              <th class="col-md-1 text-center no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>

                  </div>
              </div>
          </div>
    </div>

</div>

<script src="<?php echo JS_DIR ?>components/reference/region_list.js"></script>
