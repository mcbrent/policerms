<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
    <div class="col-lg-12 text-right">
      <button class="btn btn-primary has-tooltip form-submit" title="Save" data-form="main-form"><i class="fa fa-file"></i></button>
    </div>
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5><?php echo ucfirst($action) ?> Barangay</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="fullscreen-link">
                        <i class="fa fa-expand"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
              <form id="main-form" action="" method="POST" class="form-horizontal">
                  <h5>City</h5>
                  <div class="form-group">
                      <div class="col-sm-12">
                          <select class="select2-basic form-control" name="brgy_city_id" data-placeholder="City">
                                <option></option>
                              <?php foreach($city_list as $city){?>
                                <option value="<?php echo $city['city_id'] ?>" <?php echo ($city['city_id'] == $form_data['brgy_city_id']) ? 'selected' : '' ?>><?php echo $city['city_name'] ?></option>
                              <?php }?>
                          </select>
                      </div>
                  </div>
                  <h5>Barangay Name</h5>
                  <div class="form-group">
                      <div class="col-sm-12"><input type="text" name="brgy_name" class="form-control" placeholder="Barangay Name" value="<?php echo $form_data['brgy_name']?>"></div>
                  </div>
                </form>
            </div>
        </div>
    </div>
  </div>


</div>
