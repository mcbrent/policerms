<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <div class="row">
                        <div class="col-md-1">
                          <h5>Resident List</h5>
                        </div>
                        <div class="col-md-6">
                        <?php if(empty($brgy)){ ?>
                            <select class="form-control select2-basic" name="barangay-list">
                              <?php if(empty($brgy)){ ?>
                                <option value="0">All Barangay</option>
                                <?php foreach($brgy_list as $brgy){ ?>
                                <option value="<?php echo $brgy['brgy_id'] ?>"><?php echo $brgy['brgy_name'] ?></option>
                                <?php } ?>
                              <?php }
                              else{ ?>
                                <option value="<?php echo $brgy['brgy_id'] ?>"><?php echo $brgy['brgy_name'] ?></option>
                              <?php }
                              ?>
                            </select>
                        <?php }
                        else{ ?>
                            <p>Sort By: <?php echo $brgy['brgy_name'] ?></p>
                            <input type="hidden" name="barangay-list" value="<?php echo $brgy['brgy_id'] ?>"/>
                        <?php }
                        ?>

                        </div>
                      </div>

                      <div class="ibox-tools">
                          <a class="collapse-link">
                              <i class="fa fa-chevron-up"></i>
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content">

                    <div class="table-responsive">
                      <table id="resident-list" class="table table-striped table-bordered table-hover" data-view-method="<?php echo $view_only ?>" data-config="{}">
                        <thead>
                          <tr>
                            <th class="col-md-2 text-center">Barangay</th>
                            <th class="col-md-4 text-center">Resident</th>
                            <th class="col-md-1 text-center">Gender / Age</th>
                            <th class="col-md-1 text-center">Nationality / Civil Status</th>
                            <th class="col-md-1 text-center">Contact Info</th>
                            <th class="col-md-1 text-center">Legal Case Involved</th>
                            <th class="col-md-1 text-center no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>

                  </div>
              </div>
          </div>
    </div>

</div>

<script src="<?php echo JS_DIR ?>components/resident/resident_list.js"></script>
