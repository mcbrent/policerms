<div class="wrapper wrapper-content">
  <div class="row">

    <div class="col-lg-10">
      <h5><?php echo ucfirst($action) ?> Resident</h5>
    </div>

    <div class="col-lg-2 text-right">
      <button class="btn btn-info dim has-tooltip form-submit" name="save" title="Add Resident" data-form="main-form"><i class="fa fa-<?php if($action == 'add'){ echo "file"; } else if($action == 'edit'){ echo "pencil"; } ?>"></i> <?php echo $action ?> Resident</button>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="tabs-container">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab-basic-info"> <i class="fa fa-address-card"></i> Basic Information</a></li>
              <li class=""><a data-toggle="tab" href="#tab-additional-info" id="additional-info-button"><i class="fa fa-archive"></i> Additional Info</a></li>
              <li class=""><a data-toggle="tab" href="#tab-reports"><i class="fa fa-newspaper-o"></i> Report</a></li>
              <li class=""><a data-toggle="tab" href="#tab-attachment"><i class="fa fa-cloud"></i> Attachment</a></li>
          </ul>
          <form id="main-form" method="POST" enctype="multipart/form-data" action="?">
            <div class="tab-content">
                <div id="tab-basic-info" class="tab-pane active">
                    <div class="panel-body form-horizontal">
                      <h5>First Name, Middle Name, Last Name, Name Extension</h5>
                      <div class="form-group">
                          <div class="col-sm-3"><input name="resident-first-name" type="text" class="form-control" placeholder="First Name" value="<?php echo $form_data['resident_first_name'] ?>"></div>
                          <div class="col-sm-3"><input name="resident-middle-name" type="text" class="form-control" placeholder="Middle Name" value="<?php echo $form_data['resident_middle_name'] ?>"></div>
                          <div class="col-sm-3"><input name="resident-last-name" type="text" class="form-control" placeholder="Last Name" value="<?php echo $form_data['resident_last_name'] ?>"></div>
                          <div class="col-sm-3"><input name="resident-name-ext" type="text" class="form-control" placeholder="Name Extension" value="<?php echo $form_data['resident_name_ext'] ?>"></div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <h5>Current Address</h5>
                      <div class="form-group">
                          <div class="col-sm-12"><input type="text" name="add-desc" class="form-control" placeholder="(House/Unit No., Floor & Bldg./Street, Lot / Blk, Brgy / Village)" value="<?php echo $form_data['add_desc'] ?>"></div>
                          <div class="col-sm-3 m-t-sm">
                            <input type="text" readonly class="form-control" value="Batangas"></input>
                          </div>
                          <div class="col-sm-3 m-t-sm">
                            <input type="text" readonly class="form-control" value="Batangas City"></input>
                          </div>
                          <div class="col-sm-6 m-t-sm">
                            <select class="form-control" name="current-barangay" data-placeholder="Select Barangay">
                                <option></option>
                                <?php if($form_data['address_brgy']['brgy_id']){ ?>
                                <option value="<?php echo $form_data['address_brgy']['brgy_id'] ?>" selected><?php echo $form_data['address_brgy']['brgy_name'] ?></option>
                                <?php } ?>
                            </select>
                          </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <div class="form-group">
                        <div class="col-sm-3">
                          <h5>Gender</h5>
                          <label class="checkbox-inline i-checks"> <input type="radio" value="male" name="resident-gender" checked> <i></i> Male </label>
                          <label class="checkbox-inline i-checks"> <input type="radio" value="female" name="resident-gender" <?php echo $form_data['resident_gender'] == 'female'? 'checked': '' ?>> <i></i> Female</label>
                        </div>
                        <div class="col-sm-3">
                          <h5>Birthday <span id="resident-age"></span></h5>
                          <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="resident-birthday" class="form-control datepicker" value="<?php echo $form_data['resident_birthday'] ?>">
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <h5>Civil Status</h5>
                          <select class="form-control select2-basic" name="resident-civil-status" data-placeholder="Select Civil Status">
                              <option></option>
                              <option value="single" <?php echo $form_data['resident_civil_status'] == 'single'? 'selected' : '' ?>>Single</option>
                              <option value="married" <?php echo $form_data['resident_civil_status'] == 'married'? 'selected' : '' ?>>Married</option>
                              <option value="widowed" <?php echo $form_data['resident_civil_status'] == 'widowed'? 'selected' : '' ?>>Widowed</option>
                          </select>
                        </div>
                        <div class="col-sm-3">
                          <h5>Nationality</h5>
                          <input name="resident-nationality" type="text" class="form-control" placeholder="Enter Nationality" value="<?php echo $form_data['resident_nationality'] ?>">
                        </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <h4>Contact Details</h4>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <h5>Contact Number 1</h5>
                          <input name="phone-number-1" type="text" class="form-control" placeholder="Enter Main Contact Number" value="<?php echo $form_data['phone_number_1'] ?>">
                        </div>
                        <div class="col-sm-4">
                          <h5>Contact Number 2</h5>
                          <input name="phone-number-2" type="text" class="form-control" placeholder="Enter Mobile Number" value="<?php echo $form_data['phone_number_2'] ?>">
                        </div>
                      </div>
                      <div class="hr-line-dashed"></div>
                      <h3>Important Details</h3>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <h4>Nickname/Alias</h4>
                          <input name="resident-alias" type="text" class="form-control" placeholder="Enter Nickname/Alias" value="<?php echo $form_data['resident_nickname']?>">
                        </div>
                        <div class="col-sm-4">
                          <h4>Occupation</h4>
                          <input name="resident-occupation" type="text" class="form-control" placeholder="Enter Occupation" value="<?php echo $form_data['resident_occupation']?>">
                        </div>
                        <div class="col-sm-4">
                          <h4>Religion</h4>
                          <input name="resident-religion" type="text" class="form-control" placeholder="Enter Religion" value="<?php echo $form_data['resident_religion']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-3">
                          <h4>Years in the community</h4>
                          <input name="resident-yrs-community" type="number" min="0" max="100" class="form-control" placeholder="Enter Years in the community" value="<?php echo $form_data['resident_yrs_community']?>">
                        </div>
                        <div class="col-sm-3">
                          <h4>Weight</h4>
                          <input name="resident-weight" type="number" class="form-control" min="0" placeholder="Enter Weight" value="<?php echo $form_data['resident_weight']?>">
                        </div>
                        <div class="col-sm-3">
                          <h4>Height</h4>
                          <input name="resident-height" type="number" class="form-control" min="0" placeholder="Enter Height" value="<?php echo $form_data['resident_height']?>">
                        </div>
                        <div class="col-sm-3">
                          <h4>Completion</h4>
                          <select class="form-control select2-basic" name="resident-completion" data-placeholder="Select Completion">
                              <option></option>
                              <option value="fair" <?php echo $form_data['resident_completion'] == 'fair'? 'selected' : '' ?>>fair</option>
                              <option value="light" <?php echo $form_data['resident_completion'] == 'light'? 'selected' : '' ?>>light</option>
                              <option value="brown" <?php echo $form_data['resident_completion'] == 'brown'? 'selected' : '' ?>>brown</option>
                              <option value="dark" <?php echo $form_data['resident_completion'] == 'dark'? 'selected' : '' ?>>dark</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-4">
                          <h4>Hair color</h4>
                          <select class="form-control select2-basic" name="resident-hair-color" data-placeholder="Select Hair Color">
                              <option></option>
                              <option value="BLACK" <?php echo $form_data['resident_hair_color'] == 'BLACK'? 'selected' : '' ?>>BLACK</option>
															<option value="BALD" <?php echo $form_data['resident_hair_color'] == 'BALD'? 'selected' : '' ?>>BALD</option>
															<option value="BROWN" <?php echo $form_data['resident_hair_color'] == 'BROWN'? 'selected' : '' ?>>BROWN</option>
															<option value="BLONDE" <?php echo $form_data['resident_hair_color'] == 'BLONDE'? 'selected' : '' ?>>BLONDE</option>
															<option value="BLUE" <?php echo $form_data['resident_hair_color'] == 'BLUE'? 'selected' : '' ?>>BLUE</option>
															<option value="BRONZE" <?php echo $form_data['resident_hair_color'] == 'BRONZE'? 'selected' : '' ?>>BRONZE</option>
															<option value="DARK BROWN" <?php echo $form_data['resident_hair_color'] == 'DARK BROWN'? 'selected' : '' ?>>DARK BROWN</option>
															<option value="GREEN" <?php echo $form_data['resident_hair_color'] == 'GREEN'? 'selected' : '' ?>>GREEN</option>
															<option value="GRAY" <?php echo $form_data['resident_hair_color'] == 'GRAY'? 'selected' : '' ?>>GRAY</option>
															<option value="RED" <?php echo $form_data['resident_hair_color'] == 'RED'? 'selected' : '' ?>>RED</option>
															<option value="WHITE" <?php echo $form_data['resident_hair_color'] == 'WHITE'? 'selected' : '' ?>>WHITE</option>
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <h4>Eye color</h4>
                          <select class="form-control select2-basic" name="resident-eye-color" data-placeholder="Select Eye Color">
                              <option></option>
                              <option value="BLACK" <?php echo $form_data['resident_eye_color'] == 'BLACK'? 'selected' : '' ?>>BLACK</option>
															<option value="BROWN" <?php echo $form_data['resident_eye_color'] == 'BROWN'? 'selected' : '' ?>>BROWN</option>
															<option value="BLUE" <?php echo $form_data['resident_eye_color'] == 'BLUE'? 'selected' : '' ?>>BLUE</option>
															<option value="GREEN" <?php echo $form_data['resident_eye_color'] == 'GREEN'? 'selected' : '' ?>>GREEN</option>
															<option value="GRAY" <?php echo $form_data['resident_eye_color'] == 'GRAY'? 'selected' : '' ?>>GRAY</option>
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <h4>Body Size</h4>
                          <select class="form-control select2-basic" name="resident-body-size" data-placeholder="Select Body Size">
                              <option></option>
                              <option value="Extra extra large" <?php echo $form_data['resident_body_size'] == 'Extra extra large'? 'selected' : '' ?>>Extra extra large</option>
        											<option value="Extra large" <?php echo $form_data['resident_body_size'] == 'Extra large'? 'selected' : '' ?>>Extra large</option>
        											<option value="Large" <?php echo $form_data['resident_body_size'] == 'Large'? 'selected' : '' ?>>Large</option>
        											<option value="Medium" <?php echo $form_data['resident_body_size'] == 'Medium'? 'selected' : '' ?>>Medium</option>
        											<option value="Small" <?php echo $form_data['resident_body_size'] == 'Small'? 'selected' : '' ?>>Small</option>
        											<option value="Extra Small" <?php echo $form_data['resident_body_size'] == 'Extra Small'? 'selected' : '' ?>>Extra Small</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-12">
                          <h4>Distinguishing Marks</h4>
                          <input name="resident-distinguishing-marks" type="text" class="form-control" placeholder="Enter Distinguishing Marks" value="<?php echo $form_data['resident_distinguishing_marks']?>">
                        </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12 text-center">
                              <button id="next-basic-info" class="btn btn-success" type="button" onclick="$('.nav-tabs a[href=&quot;#tab-additional-info&quot;]').tab('show');">Next <i class="fa fa-arrow-right"></i></button>
                          </div>
                      </div>
                    </div>
                </div>
                <div id="tab-additional-info" class="tab-pane">
                    <div class="panel-body form-horizontal">
                        <h4>Additional Info</h4>
                        <div class="form-group">
                          <div class="col-sm-12">
                            <textarea name="resident-additional-info" class="summernote">
                              <?php echo $form_data['resident_add_info'] ?>
                            </textarea>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 text-center">
                                <?php if($action != 'add'){ ?>
                                <button class="btn btn-success" type="button" onclick="$('.form-submit').click() ;">SAVE <i class="fa fa-save"></i></button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-reports" class="tab-pane">
                    <div class="panel-body">
                      <?php if($action != 'add'){ ?>
                      <div class="table-responsive">
                        <table id="complaint-list" class="table table-striped table-bordered table-hover" data-config="{}">
                          <thead>
                            <tr>
                              <th class="col-md-2 text-center">Date</th>
                              <th class="col-md-5 text-center">Complaint</th>
                              <th class="col-md-2 text-center">Type</th>
                              <th class="col-md-2 text-center">Resolved</th>
                              <th class="col-md-1 text-center no-sort">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach($form_data['complaint'] as $complaint){ ?>
                            <tr>
                              <td><?php echo date('F j, Y', strtotime($complaint['complaint_date_filed']))?></td>
                              <td><?php echo $complaint['complaint_title'] ?></td>
                              <td><?php echo $complaint['complaint_type'] ?></td>
                              <td><?php echo ($complaint['complaint_is_resolved'])? 'Yes' : 'No' ?></td>
                              <td>
                                <div class="text-center">
                                  <a class="btn btn-sm btn-success has-tooltip" title="" href="<?php echo DOMAIN.'complaint/view-complaint/'.$complaint['complaint_id'] ?>" data-original-title="View"><i class="fa fa-search"></i></a>
                                  <a class="btn btn-sm btn-info has-tooltip" title="Edit" href="<?php echo DOMAIN.'complaint/edit/'.$complaint['complaint_id'] ?>"><i class="fa fa-pencil"></i></a>
                                  <button class="btn btn-sm btn-danger has-tooltip delete-row" title="Delete" value="<?php echo $complaint['complaint_id']; ?>"><i class="fa fa-trash"></i></button>
                                </div>
                              </td>
                            </tr>
                          <?php } ?>
                          </tbody>
                        </table>
                      </div>
                      <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                          <a type="button" class="btn btn-warning btn-block btn-outline dim" href="<?php echo DOMAIN.'complaint/add/'.$form_data['resident_id']?>"><i class="fa fa-gavel"></i> Create Complaint</a>
                        </div>
                      </div>
                    </div>
                    <?php } ?>
                </div>
                <div id="tab-attachment" class="tab-pane">
                    <div class="panel-body">
                      <?php if($action != 'add'){ ?>
                      <div class="table-responsive">
                        <table id="attachment-table" class="table table-striped table-bordered table-hover" data-config="{}">
                          <thead>
                            <tr>
                              <th class="col-md-6 text-center">File Name</th>
                              <th class="col-md-4 text-center">Date</th>
                              <th class="col-md-2 text-center no-sort">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                              foreach($form_data['resident_attachment'] as $attachment){ ?>
                            <tr>
                              <td class="col-md-6"><?php echo $attachment['rattachment_name']?></td>
                              <td class="col-md-4"><?php echo date('M d Y', strtotime($attachment['rattachment_date']))?></td>
                              <td class="col-md-2 text-center">
                                  <button type="button" class="btn btn-danger has-tooltip delete-row" title="Delete" value="<?php echo $attachment['rattachment_id'] ?>"><i class="fa fa-trash"></i></button>
                                  <button type="button" class="btn blue-bg has-tooltip save-file" title="Download" value="<?php echo $attachment['rattachment_id'] ?>"><i class="fa fa-download"></i></button>
                              </td>
                            </tr>
                            <?php
                              }
                            ?>
                            <tr>
                              <td class="col-md-6 text-center">
                                <input name="file-name[]" type="text" class="form-control" placeholder="Enter File Tag Name" value=""/>
                              </td>
                              <td class="col-md-4 text-center"></td>
                              <td class="col-md-2 text-center">
                                <input type="file" name="file-attachment[]" />
                              </td>
                            </tr>
                          </tbody>
                          <tfoot>
                            <td class="col-md-12 text-center gray-bg" colspan="3">
                              <button type="button" class="btn btn-primary btn-sm add-attachment"><i class="fa fa-plus"></i> Add More</button>
                            </td>
                          </tfoot>
                        </table>
                      </div>
                      <?php } ?>
                    </div>
                </div>
            </div>
          </div>
      </div>
    </div>
  </div>


</div>
<script src="<?php echo JS_DIR ?>components/resident/resident_profile.js"></script>
