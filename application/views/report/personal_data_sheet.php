<link href="<?php echo THEME; ?>css/bootstrap.min.css" rel="stylesheet">
<style>
#profile-img{
  width: 200px;
}
#main{
  width:100%
}

table, table tr{
  width:100%
}
hr {
  color:gray;
}

</style>
<htmlpagefooter name="footer">
  <div class="text-center text-muted">
    <small><u>*This document is not valid for Police Clearance and this document is intended for Internal Purpose only!</u></small>
  </div>
</htmlpagefooter>
<htmlpageheader name="header">
  <div class="text-center text-muted">
  </div>
</htmlpageheader>

<sethtmlpagefooter name="footer" value="on">
<sethtmlpageheader name="header" value="on">
<div class="header">

  <h4 class="text-center">Police Clearance Application Sheet</h4>
</div>
<hr>
<div id="main" class="container-fluid">
  <table>
    <tbody>
      <tr>
        <td colspan="7">
          <h6 class="text-muted">Full Name (First Name / Middle Name / Last Name / Name Extension)</h6>
        </td>
      </tr>
      <tr>
        <td>
          <h4 class="text-uppercase font-bold"><u><?php echo $basic_contact['bc_first_name'] ?></u></h4>
        </td>
        <td><h4>/</h4></td>
        <td>
          <h4 class="text-uppercase font-bold"><u><?php echo $basic_contact['bc_middle_name'] ?></u></h4>
        </td>
        <td><h4>/</h4></td>
        <td>
          <h4 class="text-uppercase font-bold"><u><?php echo $basic_contact['bc_last_name'] ?></u></h4>
        </td>
        <td><h4>/</h4></td>
        <td>
          <h4 class="text-uppercase font-bold"><u><?php echo ($basic_contact['bc_name_ext'])? $basic_contact['bc_name_ext'] : '(N/A)' ?></u></h4>
        </td>
      </tr>
    </tbody>
  </table>
  <hr>
  <table>
    <tbody>
      <tr>
        <td class="text-muted"><h6>Complete Address</h6></td>
      </tr>
      <tr>
        <td><span class="text-uppercase"><u><?php echo $address['address_desc']?>, Barangay <?php echo $barangay['brgy_name']?>, Batangas City, Batangas</u></span></td>
      </tr>
    </tbody>
  </table>
  <hr>
  <h5>Basic Information</h5>
  <table>
    <tbody>
      <tr>
        <td class="text-right text-muted" style="width:23%">Contact Number Primary</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $basic_contact['bc_phone_num1']?></td>
        <td class="text-right text-muted" style="width:23%">Contact Number Optional</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $basic_contact['bc_phone_num2']?></td>
      </tr>
      <tr>
        <td class="text-right text-muted" style="width:23%">Gender</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $basic_contact['bc_gender']?></td>
        <td class="text-right text-muted" style="width:23%">Age</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><span class="font-bold"><?php echo date_diff(date_create($resident['resident_birthdate']), date_create('now'))->y?> YEARS OLD</span></td>
      </tr>
      <tr>
        <td class="text-right text-muted" style="width:23%">Civil Status</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $resident['resident_civil_status']?></td>
        <td class="text-right text-muted" style="width:23%">Birthdate</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><span class="font-bold"><?php echo date('m/d/Y', strtotime($resident['resident_birthdate']))?></span></td>
      </tr>
      <tr>
        <td class="text-right text-muted" style="width:23%">Nationality</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $resident['resident_civil_status']?></td>
        <td class="text-right text-muted" style="width:23%">Occupation</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><span class="font-bold"><?php echo $resident['resident_occupation']?></span></td>
      </tr>
      <tr>
        <td class="text-right text-muted" style="width:23%">Religion</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $resident['resident_religion']?></td>
        <td class="text-right text-muted" style="width:23%">Years in the Community</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><span class="font-bold"><?php echo $resident['resident_yrs_community']?> YEARS</span></td>
      </tr>
    </tbody>
  </table>
  <hr>
  <h5>Physical Apperance</h5>
  <table>
    <tbody>
      <tr>
        <td class="text-right text-muted" style="width:23%">Weight</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $resident['resident_weight']?> kg</td>
        <td class="text-right text-muted" style="width:23%">Height</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $resident['resident_height']?> cm</td>
      </tr>
      <tr>
        <td class="text-right text-muted" style="width:23%">Completion</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $resident['resident_completion']?></td>
        <td class="text-right text-muted" style="width:23%">Body Size</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><span class="font-bold"><?php echo $resident['resident_body_size'] ?></span></td>
      </tr>
      <tr>
        <td class="text-right text-muted" style="width:23%">Hair Color</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $resident['resident_hair_color']?></td>
        <td class="text-right text-muted" style="width:23%">Eye Color</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><span class="font-bold"><?php echo $resident['resident_eye_color'] ?></span></td>
      </tr>
      <tr>
        <td class="text-right text-muted" style="width:23%">Distinguishing Marks</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><?php echo $resident['resident_distinguishing_marks']?></td>
        <td class="text-right text-muted" style="width:23%">Birthdate</td>
        <td class="text-right" style="width:6%"></td>
        <td class="text-left text-uppercase" style="width:23%"><span class="font-bold"><?php echo date('m/d/Y', strtotime($resident['resident_birthdate']))?></span></td>
      </tr>
    </tbody>
  </table>
  <hr>
  <br>
  <p class="text-center">
    <?php echo ($basic_contact['bc_gender'] == 'male')? 'Mr.' : 'Ms./Mrs.'?> <b><u><?php echo $basic_contact['bc_first_name'].' '.$basic_contact['bc_middle_name'].' '.$basic_contact['bc_last_name'].' '.$basic_contact['bc_name_ext'] ?></u></b>
    has <u><?php echo ($noOfComplaint == 0)? 'NO' : $noOfComplaint ?></u> Complaint/s as of today <u>(<?php echo date('F d Y')?>). </u>
  </p>

</div>
