<link href="<?php echo THEME; ?>css/bootstrap.min.css" rel="stylesheet">
<style>
#profile-img{
  width: 200px;
}
#main{
  width:100%;
  height:100%;
  background-color: #f4f8ff;
  background-image: url("http://localhost/policerms/assets/img/pnp_logo.png");
  background-repeat: no-repeat;
  background-position: center;
  background-attachment: fixed;
}

.pnp-logo{
  width:100%;
  height:100%;
  background-image: linear-gradient(rgba(255,255,255,0.4), rgba(255,255,255,0.4)),  url("http://localhost/policerms/assets/img/pnp_logo.png");
}

table, table tr{
  width:100%
}
hr {
  color:gray;
}
.text-bold{
  font-weight: bold;
}
#details .bg-red td {
    background-color: #f7cdca !important;
}

.emphasize{
  font-size: 18px;
  font-weight: bold;
}
</style>


<div id="main" class="container-fluid">
  <div class="pnp-logo">
    <div class="container">
      <table>
        <tr style="width:100%; height: 10%">
          <td class="text-center" style="width:25%">
            <img class="" height="100" src="<?php echo IMG_DIR?>pnp_logo.png">
          </td>
          <td class="container-fluid text-center" style="width:50%">
            <div>
              <h5>Republic of the Philippines</h5>
              <h5 class="text-bold">DEPARTMENT OF INTERIOR AND LOCAL GOVERNMENT</h5>
              <h6>National Police Commission</h6>
              <h5>PHILIPPINE NATIONAL POLICE</h5>
              <h5>BATANGAS CITY POLICE STATION</h5>
              <h5>City of Batangas</h5>
            </div>
          </td>
          <td class="text-center" style="width:25%">
            <img class="" height="100" src="<?php echo IMG_DIR?>Batangas_City_Seal.png">
          </td>
        </tr>
      </table>
      <h4 class="text-center"><u>POLICE CLEARANCE</u></h4>
      <table>
        <tr style="width:100%; height: 10%">
          <td style="width:75%">
            <table  id="details" class="table table-bordered">
              <tbody>
                <tr class="bg-red" style="width:100%">
                  <td style="width:20%" class="text-center text-bold">Control No</td>
                  <td style="width:15%" class="text-center text-bold">OR No</td>
                  <td style="width:15%" class="text-center text-bold">CTC No</td>
                  <td style="width:20%" class="text-center text-bold">Date Issued</td>
                  <td style="width:30%" class="text-center text-bold">Place Issues</td>
                </tr>
                <tr>
                  <td class="text-center red-bg"><?php echo $params['pc-control-no']?></td>
                  <td class="text-center"><?php echo $params['pc-or-no']?></td>
                  <td class="text-center"><?php echo $params['pc-ctc-no']?></td>
                  <td class="text-center"><?php echo $params['pc-date-issued']?></td>
                  <td class="text-center"><?php echo $params['pc-place-issued']?></td>
                </tr>
              </tbody>
            </table>
            <div class="emphasize">TO WHOM IT MAY CONCERN:</div>
            <p>This is to certify that the person whose names, signature, picture and thumb prints appear hereon has requested a RECORD CLEARANCE from this office and the result(s) is are listed below:</p>
            <table  id="details" class="">
              <tbody>
                <tr style="width:100%">
                  <td style="width:30%">NAME</td>
                  <td style="width:10%">:</td>
                  <td style="width:60%" class="text-uppercase emphasize"><?php echo $params['pc-name']?></td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">ALIAS</td>
                  <td style="width:10%">:</td>
                  <td style="width:60%" class="text-uppercase"><?php echo $params['pc-alias']?></td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">ADDRESS</td>
                  <td style="width:10%">:</td>
                  <td style="width:60%" class="text-uppercase"><?php echo $params['pc-address']?></td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">DATE OF BIRTH</td>
                  <td style="width:10%">:</td>
                  <td style="width:60%"><?php echo $params['pc-birthday']?></td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">PLACE OF BIRTH</td>
                  <td style="width:10%">:</td>
                  <td style="width:60%" class="text-uppercase"><?php echo $params['pc-place-birth']?></td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">GENDER</td>
                  <td style="width:10%">:</td>
                  <td style="width:60%" class="text-uppercase"><?php echo $params['pc-gender']?></td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">CIVIL STATUS</td>
                  <td style="width:10%">:</td>
                  <td style="width:60%" class="text-uppercase"><?php echo $params['pc-civil-status']?></td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">CITIZENSHIP</td>
                  <td style="width:10%">:</td>
                  <td style="width:60%" class="text-uppercase"><?php echo $params['pc-citizenship']?></td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">REMARKS</td>
                  <td style="width:10%">:</td>
                  <td style="width:60%" class="text-uppercase emphasize"><?php echo $params['pc-remarks']?></td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">VALID UNTIL</td>
                  <td style="width:10%">:</td>
                  <td style="width:60%"><?php echo $params['pc-valid']?></td>
                </tr>
              </tbody>
            </table>
            <br>
            <p>THIS CERTIFICATE IS ISSUED FOR <u><?php echo $params['pc-purpose']?></u></p>
            <br><br>
            <table  id="details" class="">
              <tbody>
                <tr style="width:100%">
                  <td style="width:30%">&nbsp;</td>
                  <td style="width:40%" class="text-uppercase text-center">APPROVED FOR ISSUE:</td>
                  <td style="width:30%">&nbsp;</td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">&nbsp;</td>
                  <td style="width:40%" class="text-center">
                    <br>
                    <img class="" height="30" src="<?php echo IMG_DIR?>signature.png">
                    <div class="text-center">
                      <p class="text-center">PSUPT SANCHO O CELEDIO</p>
                      <p class="text-center">Chief of Police</p>
                      <p class="text-center">Batangas - CPS</p>
                    </div>
                  </td>
                  <td style="width:30%">&nbsp;</td>
                </tr>
                <tr style="width:100%">
                  <td style="width:30%">
                    <br>
                    <br><small>OPERATOR: PNP BATANGAS BATANGAS</small>
                    <br><small>DATE ISSUED: <?php echo date('Y-m-d')?></small>
                    <br><small>DATE PRINTED: <?php echo date('Y-m-d')?></small>
                  </td>
                  <td style="width:40%"></td>
                  <td style="width:30%">&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </td>

          <td style="width:25%" class="text-center">
            <br>
            <br>
            <img class="" height="150" src="<?php echo UPLOAD?>profile/<?php echo $img_dir['image_name'] ?>">

            <br>
            <br>
            <table class="table table-bordered">
              <tbody>
                <tr style="width:100%">
                  <td style="background-color: white; height: 180; width: 50%">&nbsp;</td>
                  <td style="background-color: white; height: 180; width: 50%">&nbsp;</td>
                </tr>
                <br>
                <br>

              </tbody>
            </table>
            <br>
            <br>
            <br>
            <br>
            <div style="border-top: 1px solid black">APPLICANT SIGNATURE</div>
          </td>
        </tr>
        <tr style="width:100%; height: 10%">
          <td style="width:80%">

          </td>
          <td style="width:20%" class="text-center">


          </td>
        </tr>
      </table>
    </div>
  </div>


</div>
