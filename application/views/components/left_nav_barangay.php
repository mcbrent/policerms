<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li>
                <a href=""><i class="fa fa-user"></i> <span class="nav-label"> Users</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo DOMAIN; ?>users/barangayuser_list">Barangay User</a></li>
                </ul>
            </li>
            <li>
                <a href=""><i class="fa fa-address-card"></i> <span class="nav-label"> Resident</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo DOMAIN; ?>resident/list">Resident In My Barangay</a></li>
                </ul>
            </li>
            <li>
                <a href=""><i class="fa fa-gavel"></i> <span class="nav-label"> Complaint</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo DOMAIN; ?>complaint/complaint_list">Complaint List</a></li>
                </ul>
            </li>
            <li>
                <a href=""><i class="fa fa-id-badge"></i> <span class="nav-label"> Barangay Clearance</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo DOMAIN; ?>brgyclearance/list">Barangay Clearance List</a></li>
                </ul>
            </li>
        </ul>

    </div>
</nav>
