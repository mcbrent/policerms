<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="description" content="Process Police Clearance Online in just 4 steps - Batangas City">
    <meta name="keywords" content="Batangas City, Police Clearance,Batangas City Police Clearance, Batangas City Clearance">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo IMG_DIR ?>favicon.ico" sizes="16x16 32x32" type="image/ico">
    <title><?php echo $title; ?></title>

    <link href="<?php echo CSS_DIR; ?>style.css" rel="stylesheet">
    <link href="<?php echo THEME; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo THEME; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo THEME; ?>css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo THEME; ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo THEME; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo THEME ?>css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="<?php echo THEME; ?>css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="<?php echo THEME; ?>css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo THEME; ?>css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo THEME ?>css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="<?php echo THEME ?>css/plugins/summernote/summernote-bs3.css" rel="stylesheet">



    <script src="<?php echo THEME ?>js/jquery-3.1.1.min.js"></script>
    <script>var global = {site_name: '<?php echo DOMAIN; ?>'}</script>
</head>
