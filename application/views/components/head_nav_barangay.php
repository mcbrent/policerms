<div class="row border-bottom">
  <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
  <div class="navbar-header">
    <a href="<?php echo DOMAIN; ?>dashboard" class="navbar-brand blue-bg">Police Clearance</a>
      <!-- <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a> -->
  </div>
      <ul class="nav navbar-top-links navbar-right">
          <li>Barangay: <b><?php echo $current_barangay['brgy_name'] ?></b></li>
          <li>&nbsp;</li>
          <li>
              <a href="<?php echo DOMAIN?>logout">
                  <i class="fa fa-sign-out"></i> Log out
              </a>
          </li>
      </ul>

  </nav>
</div>
