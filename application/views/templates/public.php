<body class="top-navigation">
  <div id="wrapper">
      <div id="page-wrapper">
          <div class="row border-bottom">
          <nav class="navbar navbar-static-top" role="navigation">
              <nav class="navbar navbar-static-top" role="navigation">
                  <nav id="navigation" class="navbar" role="navigation">
                    <ul class="nav navbar-top-links navbar-left">
                        <li>
                            <a href="<?php echo DOMAIN; ?>" class="navbar-brand">Police Clearance</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="<?php echo DOMAIN?>login">
                                <i class="fa fa-sign-in"></i> Log in
                            </a>
                        </li>
                    </ul>
                  </nav>
              </nav>
              <!--<div class="navbar-header">
                  <button aria-controls="navbar" aria-expanded="false" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                      <i class="fa fa-reorder"></i>
                  </button>
                  <a href="<?php echo DOMAIN; ?>" class="navbar-brand blue-bg">Police Clearance</a>
              </div>
              <div class="navbar-collapse collapse" id="navbar">
                  <ul class="nav navbar-top-links navbar-right">
                      <li>
                          <a href="<?php echo DOMAIN?>login">
                              <i class="fa fa-sign-in"></i> Log in
                          </a>
                      </li>
                  </ul>
              </div>-->
          </nav>
          </div>
          <?php $this->load->view('components/alert'); ?>
          <?php echo $content; ?>
      </div>
  </div>
  <?php $this->load->view('components/javascript'); ?>
</body>
