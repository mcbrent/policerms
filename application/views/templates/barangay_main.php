<body class="mini-navbar">
  <div id="wrapper">
    <?php $this->load->view('components/left_nav_barangay'); ?>
    <div id="page-wrapper">
      <?php $this->load->view('components/head_nav_barangay', $ref); ?>
      <?php $this->load->view('components/alert'); ?>
      <?php echo $content; ?>

    </div>
  </div>
  <?php $this->load->view('components/javascript'); ?>
</body>
