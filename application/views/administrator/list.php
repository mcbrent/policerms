<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Administrator List</h5>
                      <div class="ibox-tools">
                          <a class="collapse-link">
                              <i class="fa fa-chevron-up"></i>
                          </a>
                          <a class="close-link">
                              <i class="fa fa-times"></i>
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content">

                    <div class="table-responsive">
                      <table id="administrator-list" class="table table-striped table-bordered table-hover datatable-basic" data-config="{}">
                        <thead>
                          <tr>
                            <th class="col-md-3 text-center">Name</th>
                            <th class="col-md-3 text-center">User ID</th>
                            <th class="col-md-3 text-center">Gender</th>
                            <th class="col-md-1 text-center no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>

                  </div>
              </div>
          </div>
    </div>

</div>

<script src="<?php echo JS_DIR ?>components/administrator/admin_list.js"></script>
