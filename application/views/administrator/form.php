<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
    <div class="col-lg-12 text-right">
      <button class="btn btn-primary has-tooltip form-submit" name="save" title="Save" data-form="main-form"><i class="fa fa-file"></i></button>
    </div>
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5><?php echo ucfirst($action) ?> Administrator</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="fullscreen-link">
                        <i class="fa fa-expand"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
              <form id="main-form" method="POST" class="form-horizontal" data-action="<?php echo $action ?>" action="?">
                  <div class="form-group">
                      <label class="col-sm-12">First Name, Middle Name, Last Name, Name Extension</label>
                      <div class="col-sm-3"><input type="text" name="first-name" class="form-control" placeholder="First Name" value="<?php echo $form_data['admin_first_name'] ?>"></div>
                      <div class="col-sm-3"><input type="text" name="middle-name" class="form-control" placeholder="Middle Name" value="<?php echo $form_data['admin_middle_name'] ?>"></div>
                      <div class="col-sm-3"><input type="text" name="last-name" class="form-control" placeholder="Last Name" value="<?php echo $form_data['admin_last_name'] ?>"></div>
                      <div class="col-sm-3"><input type="text" name="name-ext" class="form-control" placeholder="Name Extension" value="<?php echo $form_data['admin_name_ext'] ?>"></div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-12">Gender</label>
                    <div class="col-sm-12">
                      <label class="checkbox-inline i-checks"> <input type="radio" value="male" name="gender" <?php echo $form_data['admin_gender'] == 'male'? 'checked': '' ?>> Male </label>
                      <label class="checkbox-inline i-checks"> <input type="radio" value="female" name="gender" <?php echo $form_data['admin_gender'] == 'female'? 'checked': '' ?>> Female</label>
                    </div>
                  </div>
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                      <label class="col-sm-12">Email</label>
                      <div class="col-lg-12">
                        <input type="email" name="user-email" placeholder="Email" class="form-control" value="<?php echo $form_data['admin_email'] ?>">
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-lg-12">User Name</label>
                      <div class="col-lg-12">
                        <input type="text" name="user-id" placeholder="User Name" class="form-control" value="<?php echo $form_data['user_name'] ?>">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-lg-12">Password</label>
                    <div class="col-lg-12">
                      <input type="password" name="user-password" placeholder="Password" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-lg-12">Retype Password</label>
                    <div class="col-lg-12">
                      <input type="password" name="user-repassword" placeholder="Retype Password" class="form-control">
                    </div>
                  </div>

                  <div class="hr-line-dashed"></div>
                </form>
            </div>
        </div>
    </div>
  </div>


</div>

<script src="<?php echo JS_DIR ?>components/administrator/admin_form.js"></script>
