<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-lg-3">
      <div id="sched-pc" class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-success pull-right">Today</span>
                <a href="<?php echo DOMAIN ?>clearance/request_list"><h5>Scheduled Police Clearance</h5></a>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins content-value">0/1</h1>
                <div class="stat-percent font-bold text-success"><span class="content-percent">10</span>% <i class="fa fa-check"></i></div>
                <small>Processed / Total</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Last 30 days</span>
                <h5>Active Complaint</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins content-value">0</h1>
                <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                <small>New orders</small>
            </div>
        </div>
    </div>


  </div>
</div>
<script src="<?php echo JS_DIR ?>components/home/dashboard_admin.js"></script>
