<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends Controller {

	public function __construct(){
		parent::__construct();
	}

	public function search($z){
		$usermapper = new App\Mapper\UserMapper();
	}

	public function barangay_user_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\BarangayUserMapper();

		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

		echo json_encode($result);
	}

	public function barangayuser_list(){
		$this->is_secure = true;
		$this->view('user/barangay_user_list');
	}

	public function add_barangay_user(){
		$userMapper = new App\Mapper\UserMapper();
		$barangayUserMapper = new App\Mapper\BarangayUserMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();

		if(!empty($_POST)){
				$insert_basic_contact = array(
					'bc_first_name'			=>$_POST['first-name']
				,	'bc_middle_name'	=>$_POST['middle-name']
				,	'bc_last_name'	=>$_POST['last-name']
				,	'bc_name_ext'	=>$_POST['name-ext']
				,	'bc_phone_num1'	=>''
				,	'bc_phone_num2'	=>''
				,	'bc_phone_num3'	=>''
				,	'bc_gender'	=>$_POST['gender']
				);
				$bc_id = $basicContactMapper->insert($insert_basic_contact);

				$insert_user = array(
					'user_name'			=>$_POST['user-id']
				,	'user_password'	=>Encrypt($_POST['user-password'])
				,	'user_email'	=>$_POST['user-email']
				,	'user_type'	=> 2
				);
				$user_id = $userMapper->insert($insert_user);

				$insert_bu = array(
					'bu_user_id'	=> $user_id
				,	'bu_bc_id'	=> $bc_id
				,	'bu_position'	=> $_POST['brgy-position']
				,	'bu_brgy_id'	=> $_POST['current-barangay']
				);
				$barangayUserMapper->insert($insert_bu);
				$this->set_alert(array(
					'type'=>'success'
				,	'message'=>'<i class="fa fa-user"></i> Barangay User successfully added'
				));
		}
		$data = array(
				'bu_first_name' => ''
			,	'bu_middle_name' => ''
			,	'bu_last_name' => ''
			,	'bu_name_ext' => ''
			,	'bu_gender' => 'male'
			,	'bu_email' => ''
			,	'bu_username' => ''
			,	'bu_assigned_brgy'=>array(
					'brgy_id'=>''
				,	'brgy_name'=>''
			)
			,	'bu_brgy_position'=>''
		);
		$this->_data['action'] = 'add';
		$this->_data['form_data'] = $data;

		$this->is_secure = true;
    $this->view('user/barangay_user_form');
	}

	public function edit_barangay_user($bu_id){
		$userMapper = new App\Mapper\UserMapper();
		$barangayUserMapper = new App\Mapper\BarangayUserMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();

		$bu = $barangayUserMapper->getByFilter("bu_id = '".$bu_id."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$bu['bu_bc_id']."'", true);
		$user = $userMapper->getByFilter("user_id = '".$bu['bu_user_id']."'", true);

		if(!empty($_POST)){
				$update_basic_contact = array(
					'bc_first_name'			=>$_POST['first-name']
				,	'bc_middle_name'	=>$_POST['middle-name']
				,	'bc_last_name'	=>$_POST['last-name']
				,	'bc_name_ext'	=>$_POST['name-ext']
				,	'bc_phone_num1'	=>''
				,	'bc_phone_num2'	=>''
				,	'bc_phone_num3'	=>''
				,	'bc_gender'	=>$_POST['gender']
				);
				$basicContactMapper->update($update_basic_contact, "bc_id = '".$bu['bu_bc_id']."'");

				$update_user = array(
					'user_name'			=>$_POST['user-id']
				,	'user_email'	=>$_POST['user-email']
				,	'user_type'	=> 2
				);
				if(!empty($_POST['user-password'])){
					$update_user['user_password'] = Encrypt($_POST['user-password']);
				}
				$userMapper->update($update_user, "user_id = '".$bu['bu_user_id']."'");

				$update_bu = array(
					'bu_position'	=> $_POST['brgy-position']
				,	'bu_brgy_id'	=> $_POST['current-barangay']
				);
				$barangayUserMapper->update($update_bu, "bu_id = '".$bu['bu_id']."'");
				$this->set_alert(array(
					'type'=>'success'
				,	'message'=>'<i class="fa fa-user"></i> Barangay User successfully updated'
				));
		}

		$bu = $barangayUserMapper->getByFilter("bu_id = '".$bu_id."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$bu['bu_brgy_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$bu['bu_bc_id']."'", true);
		$user = $userMapper->getByFilter("user_id = '".$bu['bu_user_id']."'", true);
		$data = array(
				'bu_first_name' => $basicContact['bc_first_name']
			,	'bu_middle_name' => $basicContact['bc_middle_name']
			,	'bu_last_name' => $basicContact['bc_last_name']
			,	'bu_name_ext' => $basicContact['bc_name_ext']
			,	'bu_gender' => $basicContact['bc_gender']
			,	'bu_email' => $user['user_email']
			,	'bu_username' => $user['user_name']
			,	'bu_assigned_brgy'=>array(
					'brgy_id'=>$brgy['brgy_id']
				,	'brgy_name'=>$brgy['brgy_name']
			)
			,	'bu_brgy_position'=>$bu['bu_position']
		);
		$this->_data['action'] = 'edit';
		$this->_data['form_data'] = $data;

		$this->is_secure = true;
    $this->view('user/barangay_user_form');
	}

	public function delete_barangay_user(){
		$param = $_POST;
		$bu_id = $param['id'];

		$barangayUserMapper = new App\Mapper\BarangayUserMapper();
		$usermapper = new App\Mapper\UserMapper();
		$basicContactmapper = new App\Mapper\BasicContactMapper();

		$barangayUser = $barangayUserMapper->getByFilter("bu_id = '". $bu_id ."' ", true);
		$barangayUserMapper->delete("bu_id = '" . $bu_id. "'");
		$usermapper->delete("user_id = '" . $barangayUser['bu_user_id']. "'");
		$basicContactmapper->delete("bc_id = '" . $barangayUser['bu_bc_id']. "'");

		echo json_encode(array(
			"success"=>1
		));
	}

}
