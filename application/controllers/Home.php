<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends Controller {

	public function __construct(){
		parent::__construct();
	}

	public function system_info(){
		echo phpinfo();
	}
	public function dashboard_catch(){
		$input = $_POST;
		$result = array();
		$PCRequestMapper = new App\Mapper\PCRequestMapper();
		$date = $input['today'];

		$currentProcessedPCRequest = $PCRequestMapper->getTotalPendingByDate($date);
		$result['sched_police_clearance'] = $currentProcessedPCRequest;
		echo json_encode($result);
	}

  public function dashboard(){
			$this->is_secure = true;
			if($_SESSION['current_user']['type'] == '1'){
				//Means Admin
				$this->_template = 'templates/admin_main';
				$this->view('home/dashboard_admin');
			}
			if($_SESSION['current_user']['type'] == '2'){
				//Means Barangay Admin
				$this->_template = 'templates/barangay_main';
				$this->view('home/dashboard_brgy');
			}

  }

	public function error_404(){
		$this->is_secure = false;
		$this->_template = 'templates/main';
		$this->view('home/error_404');
	}

	public function landing_page(){
		$this->is_secure = false;
		$this->_template = 'templates/public';
		$this->view('home/landing_page');
	}
}
