<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Brgyclearance extends Controller {

	private $_default_province_id = 24;
	private $_default_city_id = 447;

	public function __construct(){
		parent::__construct();
	}

	public function list(){
		$this->is_secure = true;
		$this->view('brgyclearance/list');
	}

	public function brgyclearance_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\BarangayClearanceMapper();
		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

		echo json_encode($result);
	}

	public function issue(){
		$barangayClearanceMapper = new App\Mapper\BarangayClearanceMapper();
		if(!empty($_POST)){

			$brgyc = array(
				'brgyc_resident_id'=>$_POST['resident-id']
			,	'brgyc_validity_from'=>date("Y-m-d", strtotime($_POST['valid-from']))
			,	'brgyc_validity_to'=>date("Y-m-d", strtotime($_POST['valid-to']))
			);

			$brgyc_id = $barangayClearanceMapper->insert($brgyc);
			$this->set_alert(array(
				'type'=>'success'
			,	'message'=>'<i class="fa fa-check-circle"></i> Barangay Clearance has been successful issued'
			));
		}


		$this->is_secure = true;
		$this->_data['action'] = 'add';
		$this->view('brgyclearance/issue');
	}

	public function delete(){
		$param = $_POST;
		$brgyc_id = $param['id'];

		$barangayClearanceMapper = new App\Mapper\BarangayClearanceMapper();

		$barangayClearanceMapper->delete("brgyc_id = '".$brgyc_id."'");

		echo json_encode(array(
			"success"=>1
		));
	}


}
