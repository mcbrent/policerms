<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Resident extends Controller {

	private $_default_province_id = 24;
	private $_default_city_id = 447;
	public function __construct(){
		parent::__construct();
	}

	public function print(){
		$filters = $_POST;

		$data = array();
		$residentMapper = new App\Mapper\ResidentMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$complaintMapper = new App\Mapper\ComplaintMapper();
		$resident = $residentMapper->getByFilter("resident_id = '".$filters['resident']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$barangay = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
		$complaint = $complaintMapper->getByFilter("complaint_is_resolved = '0'", false);
		$data['resident'] = $resident;
		$data['basic_contact'] = $basicContact;
		$data['address'] = $address;
		$data['barangay'] = $barangay;
		$data['noOfComplaint'] = count($complaint);

		// echo "<pre>";
		// print_r($basicContact);
		// echo "</pre>";
    // $data['filter'] = $filters;
    // $data['month_date'] = date('m/d/Y', strtotime('-6 months', strtotime($filters['start_date'])));
    // $data['lastyear_date'] = date('m/d/Y', strtotime('-12 months', strtotime($filters['start_date'])));
		//
    // $data['company'] = $this->daily_sales_company_model->getCompanyName($filters);
    // $data['rows'] = $this->daily_sales_company_model->getReport($filters);
		$html = $this->load->view('report/personal_data_sheet', $data, true);

    $this->load->library('MPdf');
    //echo $html;
		$this->mpdf->generate(
			array(	'format'=>'Letter',
					'orientation'=>'P',
					'html'=>$html
			));
	}

	public function save_file(){
		$option = $_POST;
		$this->load->model('FileManagement/Copy_Model');
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();

		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_id = '". $option['id']."' ", true);
		$fileManager = $fileManagerMapper->getByFilter("fm_id = '". $residentAttachment['rattachment_fm_id']."' ", true);

		$file = $this->Copy_Model->copyFile(array(
			'file_name' => empty($residentAttachment['rattachment_name'])? 'Untitled' : $residentAttachment['rattachment_name']
		,	'encrypted_name'=>$fileManager['fm_encypted_name']
		));
		echo json_encode($file);
	}

	public function delete_file(){
		$option = $_POST;
		$this->load->model('FileManagement/Upload_Model');
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();

		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_id = '". $option['id']."' ", true);
		$fileManager = $fileManagerMapper->getByFilter("fm_id = '". $residentAttachment['rattachment_fm_id']."' ", true);

		$file_path = 'upload/files/'.$fileManager['fm_encypted_name'];
		if(file_exists($file_path)){
			$this->Upload_Model->delete_file($file_path);
		}
		//echo
		$residentAttachmentMapper->delete(array(
			array(
							'column'=>'rattachment_id'
						,	'value'=> $option['id'])
		));
		$result = $fileManagerMapper->delete(array(
			array(
							'column'=>'fm_id'
						,	'value'=> $fileManager['fm_id'])
		));
		echo json_encode($result);
	}

	public function resident_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$params = isset($_POST['filter'])? $_POST['filter']: array();
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}


		$mapper = new App\Mapper\ResidentMapper();
		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders, $params);

		echo json_encode($result);
	}

	public function list(){
		$this->is_secure = true;
		$this->all_resident();
	}
	private function barangay_resident(){

	}

	private function all_resident(){
		$current_barangay = array();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$barangayList = $barangayMapper->getAll();

		if($this->sess->isLogin()){
			if($_SESSION['current_user']['type'] == '2'){
				$userMapper = new App\Mapper\UserMapper();
				$barangayUserMapper = new App\Mapper\BarangayUserMapper();
				$user = $userMapper->getByFilter("user_id = '".$_SESSION['current_user']['id']."'", true);
				$barangayUser = $barangayUserMapper->getByFilter("bu_user_id = '".$user['user_id']."'", true);
				$current_barangay = $barangayMapper->getByFilter("brgy_id = '".$barangayUser['bu_brgy_id']."'", true);
				$this->_data['view_only'] = '0';
			}
			else{
				$this->_data['view_only'] = '1';
			}

		}

		$this->_data['brgy'] = $current_barangay;
		$this->_data['brgy_list'] = $barangayList;
		$this->view('resident/list');
	}

	public function add_resident(){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$data = array(
				'resident_first_name' => ''
			,	'resident_middle_name' => ''
			,	'resident_last_name' => ''
			,	'resident_name_ext' => ''
			,	'add_desc'	=> ''
			,	'address_brgy_id' => ''
			,	'address_brgy' => array(
					'brgy_id'	=> ''
				,	'brgy_name'=> ''
				)
			,	'resident_gender'=>'male'
			,	'resident_birthday' => ''
			,	'resident_civil_status'=>''
			,	'resident_nationality'=>''
			,	'phone_number_1'=>''
			,	'phone_number_2'=>''
			,	'resident_add_info'=>''
			,	'resident_attachment'=>array()
			,	'resident_nickname'=>''
			,	'resident_occupation'=>''
			,	'resident_religion'=>''
			,	'resident_yrs_community'=>'0'
			,	'resident_weight'=>'0'
			,	'resident_height'=>'0'
			,	'resident_completion'=>''
			,	'resident_distinguishing_marks'=>''
			,	'resident_hair_color'=>''
			,	'resident_eye_color'=>''
			,	'resident_body_size'=>''
		);

		if(!empty($_POST)){
			$current_user_id = $_SESSION['current_user']['id'];

			$bc_id = $basicContactMapper->insert(array(
					'bc_first_name' => $_POST['resident-first-name']
				,	'bc_middle_name'=> $_POST['resident-middle-name']
				,	'bc_last_name'=> $_POST['resident-last-name']
				,	'bc_name_ext'=> $_POST['resident-name-ext']
				,	'bc_phone_num1'=> $_POST['phone-number-1']
				,	'bc_phone_num2'=> $_POST['phone-number-2']
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	$_POST['resident-gender']
			));

			$addressMapper = new App\Mapper\AddressMapper();
			$resident_address_id = $addressMapper->insert(array(
				'address_city_id' 		=> $this->_default_city_id
			,	'address_province_id' => $this->_default_province_id
			,	'address_brgy_id' => $_POST['current-barangay']
			,	'address_desc' => $_POST['add-desc']
			));

			$residentMapper->insert(array(
				'resident_bc_id'	=> $bc_id
			,	'resident_address_id'	=> $resident_address_id
			,	'resident_birthdate'	=> date("Y-m-d", strtotime($_POST['resident-birthday']))
			,	'resident_civil_status'	=> $_POST['resident-civil-status']
			,	'resident_nationality'	=> $_POST['resident-nationality']
			,	'resident_added_by' => $current_user_id
			,	'resident_added_time'=>  date('Y-m-d H:i:s', time())
			,	'resident_modified_by' => $current_user_id
			,	'resident_modified_time'=>  date('Y-m-d H:i:s', time())
			,	'resident_add_info'=> $_POST['resident-additional-info']
			,	'resident_nickname'=> $_POST['resident-alias']
			,	'resident_occupation'=> $_POST['resident-occupation']
			,	'resident_religion'=> $_POST['resident-religion']
			,	'resident_yrs_community'=> $_POST['resident-yrs-community']
			,	'resident_weight'=> $_POST['resident-weight']
			,	'resident_height'=> $_POST['resident-height']
			,	'resident_completion'=> $_POST['resident-completion']
			,	'resident_distinguishing_marks'=> $_POST['resident-distinguishing-marks']
			,	'resident_hair_color'=> $_POST['resident-hair-color']
			,	'resident_eye_color'=> $_POST['resident-eye-color']
			,	'resident_body_size'=> $_POST['resident-body-size']
			));

			$this->set_alert(array(
				'type'=>'success'
			,	'message'=>'<i class="fa fa-check-circle"></i> Resident successfully added'
			));

		}

		$this->_data['action'] = 'add';
		$this->_data['form_data'] = $data;
		$this->is_secure = true;
		$this->_template = 'templates/admin_main';
    $this->view('resident/form');
	}

	public function edit_resident($resident_id){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$complaintMapper = new App\Mapper\ComplaintMapper();
		$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");
		$complaint = $complaintMapper->selectComplaintByResident($resident_id);

		if(!empty($_POST)){
			$upload_list = array();

			$fileAttachment = $_FILES['file-attachment'];
			$numOfFiles = count($fileAttachment['name']);
			$this->load->model('FileManagement/Upload_Model');
			for($i=0;$i<$numOfFiles;$i++){
				array_push($upload_list, array(
					'name'=>$fileAttachment['name'][$i]
				,	'type'=>$fileAttachment['type'][$i]
				,	'tmp_name'=>$fileAttachment['tmp_name'][$i]
				,	'error'=>$fileAttachment['error'][$i]
				,	'size'=>$fileAttachment['size'][$i]
				));
			}


			$current_user_id = $_SESSION['current_user']['id'];

			$residentMapper->update(array(
					'resident_birthdate'	=>	date("Y-m-d", strtotime($_POST['resident-birthday']))
				,	'resident_civil_status'	=>	$_POST['resident-civil-status']
				,	'resident_nationality'	=>	$_POST['resident-nationality']
				,	'resident_modified_by' => $current_user_id
				,	'resident_modified_time'=>  date('Y-m-d H:i:s', time())
				,	'resident_add_info'=> $_POST['resident-additional-info']
				,	'resident_nickname'=> $_POST['resident-alias']
				,	'resident_occupation'=> $_POST['resident-occupation']
				,	'resident_religion'=> $_POST['resident-religion']
				,	'resident_yrs_community'=> $_POST['resident-yrs-community']
				,	'resident_weight'=> $_POST['resident-weight']
				,	'resident_height'=> $_POST['resident-height']
				,	'resident_completion'=> $_POST['resident-completion']
				,	'resident_distinguishing_marks'=> $_POST['resident-distinguishing-marks']
				,	'resident_hair_color'=> $_POST['resident-hair-color']
				,	'resident_eye_color'=> $_POST['resident-eye-color']
				,	'resident_body_size'=> $_POST['resident-body-size']
			), "resident_id = '".$resident['resident_id']."'");

			$basicContactMapper->update(array(
					'bc_first_name' => $_POST['resident-first-name']
				,	'bc_middle_name'=> $_POST['resident-middle-name']
				,	'bc_last_name'=> $_POST['resident-last-name']
				,	'bc_name_ext'=> $_POST['resident-name-ext']
				,	'bc_phone_num1'=> $_POST['phone-number-1']
				,	'bc_phone_num2'=> $_POST['phone-number-2']
				,	'bc_phone_num3'=> ''
				,	'bc_gender'=>	$_POST['resident-gender']
			), "bc_id = '".$resident['resident_bc_id']."'");

			$addressMapper->update(array(
					'address_brgy_id' => $_POST['current-barangay']
				,	'address_desc' => $_POST['add-desc']
			), "address_id = '".$resident['resident_address_id']."'");

			$this->load->model('FileManagement/Upload_Model');

			foreach($upload_list as $i=>$temp_file){
					if($temp_file['error'] == '0'){
						$result = $this->Upload_Model->upload_file($temp_file);
						$fm_id = $fileManagerMapper->insert(array(
							'fm_encypted_name'	=> $result['new_file_name']
						));

						$ra_id = $residentAttachmentMapper->insert(array(
							'rattachment_fm_id'=>$fm_id
						,	'rattachment_resident_id'=>$resident['resident_id']
						,	'rattachment_name'=>$_POST['file-name'][$i]
						));
					}
			}
			$this->set_alert(array(
				'type'=>'success'
			,	'message'=>'<i class="fa fa-check-circle"></i> Resident successfully updated'
			));
			$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
			$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
			$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
			$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
			$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");
		}


		$data = array(
				'resident_id' => $resident_id
			,	'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_birthday' => date("m/d/Y", strtotime($resident['resident_birthdate']))
			,	'resident_civil_status'=>$resident['resident_civil_status']
			,	'resident_nationality'=>$resident['resident_nationality']
			,	'phone_number_1'=>$basicContact['bc_phone_num1']
			,	'phone_number_2'=>$basicContact['bc_phone_num2']
			,	'resident_add_info'=>$resident['resident_add_info']
			,	'resident_attachment'=>$residentAttachment
			,	'resident_nickname'=>$resident['resident_nickname']
			,	'resident_occupation'=>$resident['resident_occupation']
			,	'resident_religion'=>$resident['resident_religion']
			,	'resident_yrs_community'=>$resident['resident_yrs_community']
			,	'resident_weight'=>$resident['resident_weight']
			,	'resident_height'=>$resident['resident_height']
			,	'resident_completion'=>$resident['resident_completion']
			,	'resident_distinguishing_marks'=>$resident['resident_distinguishing_marks']
			,	'resident_hair_color'=>$resident['resident_hair_color']
			,	'resident_eye_color'=>$resident['resident_eye_color']
			,	'resident_body_size'=>$resident['resident_body_size']
			,	'complaint'	=>$complaint
		);

		$this->_data['action'] = 'edit';
		$this->_data['form_data'] = $data;
		$this->is_secure = true;
		$this->_template = 'templates/admin_main';
    $this->view('resident/form');
	}

	public function view_resident($resident_id){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$complaintMapper = new App\Mapper\ComplaintMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();

		$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");
		$complaint = $complaintMapper->selectComplaintByResident($resident_id);

		$data = array(
				'resident_id' => $resident_id
			,	'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_birthday' => date("m/d/Y", strtotime($resident['resident_birthdate']))
			,	'resident_civil_status'=>$resident['resident_civil_status']
			,	'resident_nationality'=>$resident['resident_nationality']
			,	'phone_number_1'=>$basicContact['bc_phone_num1']
			,	'phone_number_2'=>$basicContact['bc_phone_num2']
			,	'resident_add_info'=>$resident['resident_add_info']
			,	'resident_attachment'=>$residentAttachment
			,	'resident_nickname'=>$resident['resident_nickname']
			,	'resident_occupation'=>$resident['resident_occupation']
			,	'resident_religion'=>$resident['resident_religion']
			,	'resident_yrs_community'=>$resident['resident_yrs_community']
			,	'resident_weight'=>$resident['resident_weight']
			,	'resident_height'=>$resident['resident_height']
			,	'resident_completion'=>$resident['resident_completion']
			,	'resident_distinguishing_marks'=>$resident['resident_distinguishing_marks']
			,	'resident_hair_color'=>$resident['resident_hair_color']
			,	'resident_eye_color'=>$resident['resident_eye_color']
			,	'resident_body_size'=>$resident['resident_body_size']
			,	'complaint'	=>$complaint
		);

		$this->_data['form_data'] = $data;
		$this->is_secure = true;
    $this->view('resident/view');
	}

	public function delete_resident(){
		$param = $_POST;
		$resident_id = $param['id'];

		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayClearanceMapper = new App\Mapper\BarangayClearanceMapper();
		$complaintMapper = new App\Mapper\ComplaintMapper();
		$pcRequestMapper = new App\Mapper\PCRequestMapper();

		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();

		$fmList = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");
		$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);

		foreach($fmList as $fm){
			$fileManagerMapper->delete("fm_id = '".$fm['rattachment_fm_id']."'");
		}
		$residentAttachmentMapper->delete("rattachment_resident_id = '".$resident_id."'");
		$residentMapper->delete("resident_id = '".$resident_id."'");
		$basicContactMapper->delete("bc_id = '".$basicContact['bc_id']."'");
		$addressMapper->delete("address_id = '".$address['address_id']."'");
		$barangayClearanceMapper->delete("brgyc_id = '".$resident_id."'");
		$complaintMapper->delete("complaint_resident_id = '".$resident_id."'");
		$pcRequestMapper->delete("pcq_resident_id = '".$resident_id."'");

		echo "1";
	}


	public function admin_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$option = $_POST['option'];
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\AdminMapper();

		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

		echo json_encode($result);
	}

	public function add_admin(){
		$userMapper = new App\Mapper\UserMapper();
		$adminMapper = new App\Mapper\AdminMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();

		$regionMapper = new App\Mapper\RegionMapper();
		$countryMapper = new App\Mapper\CountryMapper();
		$data = array(
				'region_id' => ''
			,	'region_country_id' => ''
			,	'region_code' => ''
			,	'region_desc' => ''
		);
		if(!empty($_POST)){
				$insert_basic_contact = array(
					'bc_first_name'			=>$_POST['first-name']
				,	'bc_middle_name'	=>$_POST['middle-name']
				,	'bc_last_name'	=>$_POST['last-name']
				,	'bc_name_ext'	=>$_POST['name-ext']
				,	'bc_phone_num1'	=>''
				,	'bc_phone_num2'	=>''
				,	'bc_phone_num3'	=>''
				,	'bc_gender'	=>$_POST['gender']
				,	'bc_email_address'	=>$_POST['user-email']
				);
				$bc_id = $basicContactMapper->insert($insert_basic_contact);

				$insert_user = array(
					'user_name'			=>$_POST['user-id']
				,	'user_password'	=>$_POST['user-password']
				,	'user_email'	=>$_POST['user-email']
				,	'user_type'	=> 1
				);
				$userMapper->insert($insert_user);

				$insert_admin = array(
					'admin_user_name'	=> $insert_user['user_name']
				,	'admin_bc_id'	=> $bc_id
				);
				$adminMapper->insert($insert_admin);

		}
		$this->_data['action'] = 'add';
		$this->_data['form_data'] = $data;

		$this->_template = 'templates/admin_main';
    $this->view('administrator/form');
	}

}
