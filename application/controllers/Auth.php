<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends Controller {

	public function __construct(){
		parent::__construct();
	}

	public function login(){
		if(!empty($_POST)){
			$userMapper = new App\Mapper\UserMapper();
			$basicContactMapper = new App\Mapper\BasicContactMapper();
			$user_email = $_POST['user_email'];
			$user_password =  encrypt($_POST['user_password']);
			$user = $userMapper->selectByLoginPassword($user_email, $user_password);
			if($user){
				$bc_id = 0;
				if($user['user_type'] == 1){
					//Admin
					$adminMapper = new App\Mapper\AdminMapper();
					$admin = $adminMapper->getByID($user['user_id']);
					$bc_id = $admin['admin_bc_id'];
				}
				if($user['user_type'] == 2){
					//Barangay User
					$barangayUserMapper = new App\Mapper\BarangayUserMapper();
					$brgy = $barangayUserMapper->getByFilter("bu_user_id = '".$user['user_id']."'", true);
					$bc_id = $brgy['bu_bc_id'];
				}
				$basicContact = $basicContactMapper->getByFilter("bc_id = '".$bc_id."'", true);
				$displayname = $basicContact['bc_first_name'] . ' ' . $basicContact['bc_middle_name'] . ' ' . $basicContact['bc_last_name'] . ' ' . $basicContact['bc_name_ext'];

				$user_details = array(
						'displayname'=>	$displayname
					,	'id'=>	$user['user_id']
					,	'email'		=>$user['user_email']
					,	'type'		=>$user['user_type']
				);
				$this->sess->setLogin($user_details);
				// $this->set_alert(array(
				// 	'message'=>"<h2><i class=\"fa fa-user-o\"></i> Hello " + $user_details['displayname'] + "</h2>"
				// ,	'type'=>'success'
				// ));
				$this->set_alert(array(
					'type'=>'success'
				,	'message'=>'<h2><i class="fa fa-user-o"></i> Welcome! ' . $displayname . '</h2>'
				));
				if(isset($_GET['redirect_url'])){
					$this->redirect($_GET['redirect_url']);
				}
				else{
					$this->redirect(DOMAIN.'dashboard');
				}
			}
			else{
				echo "Invalid User Credentials";
			}
		}
		if(!$this->sess->isLogin()){
			$this->_data['action_url'] = (isset($_GET['redirect_url']))? $_SERVER['REQUEST_URI'] : '?';
			$this->view('auth/login');
		}
		else{
			$this->redirect(DOMAIN.'dashboard');
		}
	}

	public function verify_reset(){

		$userMapper = new App\Mapper\UserMapper();
		$is_valid = false;
		if(!empty($_GET['code'])){
			$user = $userMapper->getByFilter("user_reset_code = '".$_GET['code']."'", true);
			if(!empty($user)){
				$is_valid = true;
				if(!empty($_POST)){
					$userMapper->update(array(
						'user_password' => encrypt($_POST['user_password']),
						'user_reset_code' => ''
					), "user_id = '".$user['user_id']."'" );
					$this->set_alert(array(
						'type'=>'success'
					,	'message'=>'<i class="fa fa-check"></i> Change password had been successful'
					));
					$this->redirect(DOMAIN.'login');
				}


			}
		}
		else{

		}

		$this->_data['is_valid'] = $is_valid;
		$this->view('auth/verify_reset');
	}

	public function reset_password(){

		if(!empty($_POST)){
			$userMapper = new App\Mapper\UserMapper();
			$user = $userMapper->getByFilter("user_email = '".$_POST['user_email']."'", true);
			$reset_code = md5(uniqid());
			$userMapper->update(array(
				'user_reset_code' => $reset_code
			), "user_email = '".$_POST['user_email']."'");
			$link = DOMAIN.'auth/verify-reset?code='.$reset_code;
			$this->load->library('email', $this->config->item('email'));

			$message = '<p><span style="font-size: 18px;">Click the link to reset the password of your account</span></p>
									<p><span style="font-size: 18px;"><a href="http://www.batangascitypoliceclearance.com'.$link.'">http://www.batangascitypoliceclearance.com'.$link.'</a></span></p>
									<p style="text-align: center;"><a href="http://www.batangascitypoliceclearance.com" rel="noopener noreferrer" target="_blank">www.batangascitypoliceclearance.com</a></p>
									<p style="text-align: center;">Copyright &copy; Police Clearance and Records Management - Batangas City 2018</p>';

			$this->email->from('verify@batangascitypoliceclearance.com', "Account Reset Password");
			$this->email->to($_POST['user_email']);
			$this->email->bcc($this->config->item('email')['smtp_user']);
			$this->email->subject("Reset Password");
			$this->email->message($message);
			$this->email->send();
			$this->_data['has_sent'] = true;
			$this->_data['email'] = $_POST['user_email'];
		}

		$this->view('auth/reset_password');
	}

	public function logout(){
		$this->sess->clearLogin();
		$this->redirect(DOMAIN.'login');
	}

}
