<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Complaint extends Controller {

	private $_default_province_id = 24;
	private $_default_city_id = 447;

	public function __construct(){
		parent::__construct();
	}

	public function complaint_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$params = isset($_POST['filter'])? $_POST['filter'] : array();
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\ComplaintMapper();

		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders, $params);

		echo json_encode($result);
	}

	public function add($resident_id){
		$complaintMapper = new App\Mapper\ComplaintMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();

		$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);

		$data = array(
				'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_birthday' => date("m/d/Y", strtotime($resident['resident_birthdate']))
			,	'complaint_date_filed' => date('m/d/Y')
			,	'complaint_type'=>''
			,	'complaint_title'=>''
			,	'complaint_description'=>''
		);

		if(!empty($_POST)){
			$current_user_id = $_SESSION['current_user']['id'];

			$complaint_id = $complaintMapper->insert(array(
				'complaint_resident_id'=>$resident_id
			,	'complaint_date_filed'=>date('Y/m/d', strtotime($_POST['complaint-date']))
			,	'complaint_type'=>$_POST['complaint-type']
			,	'complaint_is_resolved'=>0
			,	'complaint_title'=>$_POST['complaint-title']
			,	'complaint_description'=>$_POST['complaint-content']
			,	'complaint_user_id'=>$current_user_id
			));

			$this->set_alert(array(
				'type'=>'success'
			,	'message'=>'<i class="fa fa-gavel"></i> Complaint successful stored'
			));
			$this->redirect(DOMAIN.'complaint/edit/'.$complaint_id);

		}
		$this->_data['form_data'] = $data;
		$this->_data['action'] = 'add';
		$this->is_secure = true;
    $this->view('complaint/form');
	}
	public function edit($complaint_id){
		$complaintMapper = new App\Mapper\ComplaintMapper();
		$complaint = $complaintMapper->getByFilter("complaint_id = '".$complaint_id."'", true);
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();

		$resident = $residentMapper->getByFilter("resident_id = '".$complaint['complaint_resident_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);


		if(!empty($_POST)){
			$current_user_id = $_SESSION['current_user']['id'];

			$complaintMapper->update(array(
				'complaint_date_filed'=>date('Y/m/d', strtotime($_POST['complaint-date']))
			,	'complaint_type'=>$_POST['complaint-type']
			,	'complaint_is_resolved'=>0
			,	'complaint_title'=>$_POST['complaint-title']
			,	'complaint_description'=>$_POST['complaint-content']
			,	'complaint_user_id'=>$current_user_id
		), "complaint_id = '". $complaint_id."'");

			$this->set_alert(array(
				'type'=>'info'
			,	'message'=>'<i class="fa fa-gavel"></i> Complaint successful updated'
			));
		}

		$complaint = $complaintMapper->getByFilter("complaint_id = '".$complaint_id."'", true);
		$data = array(
				'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_birthday' => date("m/d/Y", strtotime($resident['resident_birthdate']))
			,	'complaint_date_filed' => date('m/d/Y', strtotime($complaint['complaint_date_filed']))
			,	'complaint_type'=>$complaint['complaint_type']
			,	'complaint_title'=>$complaint['complaint_title']
			,	'complaint_description'=>$complaint['complaint_description']
		);
		$this->_data['form_data'] = $data;
		$this->_data['action'] = 'edit';
		$this->is_secure = true;
    $this->view('complaint/form');
	}

	public function view_complaint($complaint_id){
		$complaintMapper = new App\Mapper\ComplaintMapper();
		$complaint = $complaintMapper->getByFilter("complaint_id = '".$complaint_id."'", true);
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();

		$resident = $residentMapper->getByFilter("resident_id = '".$complaint['complaint_resident_id']."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);

		$data = array(
				'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_birthday' => date("m/d/Y", strtotime($resident['resident_birthdate']))
			,	'complaint_date_filed' => date('m/d/Y', strtotime($complaint['complaint_date_filed']))
			,	'complaint_type'=>$complaint['complaint_type']
			,	'complaint_title'=>$complaint['complaint_title']
			,	'complaint_description'=>$complaint['complaint_description']
		);
		$this->_data['form_data'] = $data;
		$this->_data['action'] = 'edit';
		$this->is_secure = true;
    $this->view('complaint/view');
	}


	public function request_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\PCRequestMapper();

		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

		echo json_encode($result);
	}

	public function complaint_list(){
		$current_barangay = array();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$barangayList = $barangayMapper->getAll();

		if($this->sess->isLogin()){
			if($_SESSION['current_user']['type'] == '2'){
				$userMapper = new App\Mapper\UserMapper();
				$barangayUserMapper = new App\Mapper\BarangayUserMapper();
				$user = $userMapper->getByFilter("user_id = '".$_SESSION['current_user']['id']."'", true);
				$barangayUser = $barangayUserMapper->getByFilter("bu_user_id = '".$user['user_id']."'", true);
				$current_barangay = $barangayMapper->getByFilter("brgy_id = '".$barangayUser['bu_brgy_id']."'", true);
			}
		}
		$this->_data['brgy'] = $current_barangay;
		$this->_data['brgy_list'] = $barangayList;
		$this->is_secure = true;
		$this->view('complaint/list');
	}

	public function delete_complaint(){
		$param = $_POST;
		$complaint_id = $param['id'];

		$complaintMapper = new App\Mapper\ComplaintMapper();

		$complaintMapper->delete("complaint_id = '".$complaint_id."'");

		echo "1";
	}


}
