<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Clearance extends Controller {

	private $_default_province_id = 24;
	private $_default_city_id = 447;

	public function __construct(){
		parent::__construct();
	}

	public function process_clearance(){
		$input = $_POST;
		$PCRequestMapper = new App\Mapper\PCRequestMapper();
		if(!empty($input)){
			$update = array(
				'pcq_status'	=>	0
			);
			if($input['method'] == 'mark-finish'){
				$update['pcq_status']	= '3';
			}
			if($input['method'] == 'reject'){
				$update['pcq_status']	= '2';
			}
			$PCRequestMapper->update($update, "pcq_id = '".$input['pc_id']."'");
			echo "true";
		}
		else{
			echo "false";
		}


	}

	public function request_ref(){
		$limit = $_POST['length'];
		$offset = $_POST['start'];
		$search = $_POST['search'];
		$columns = $_POST['columns'];
		$orders = array();

		foreach($_POST['order'] as $_order){
			array_push($orders, array(
				'col'=> $_POST['columns'][$_order['column']]['data']
			,	'type'	=> $_order['dir']
			));
		}
		$mapper = new App\Mapper\PCRequestMapper();

		$result = $mapper->selectDataTable($search['value'], $columns, $limit, $offset, $orders);

		echo json_encode($result);
	}

	public function request_list(){
		$this->is_secure = true;
		$this->view('clearance/request_list');
	}

	public function format($pcq_id){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$pcRequestMapper = new App\Mapper\PCRequestMapper();
		$complaintMapper = new App\Mapper\ComplaintMapper();
		$pcRequest = $pcRequestMapper->getByFilter("pcq_id = '".$pcq_id ."'", true);

		$resident_id = $pcRequest['pcq_resident_id'];
		$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");
		$complaint = $complaintMapper->selectComplaintByResident($resident_id);
		$data = array(
				'pcq_id'=>$pcq_id
			,	'resident_id'=>$resident_id
			,	'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_birthday' => date("m/d/Y", strtotime($resident['resident_birthdate']))
			,	'resident_civil_status'=>$resident['resident_civil_status']
			,	'resident_nationality'=>$resident['resident_nationality']
			,	'phone_number_1'=>$basicContact['bc_phone_num1']
			,	'phone_number_2'=>$basicContact['bc_phone_num2']
			,	'resident_add_info'=>$resident['resident_add_info']
			,	'resident_attachment'=>$residentAttachment
			,	'resident_nickname'=>$resident['resident_nickname']
			,	'resident_occupation'=>$resident['resident_occupation']
			,	'resident_religion'=>$resident['resident_religion']
			,	'resident_yrs_community'=>$resident['resident_yrs_community']
			,	'resident_weight'=>$resident['resident_weight']
			,	'resident_height'=>$resident['resident_height']
			,	'resident_completion'=>$resident['resident_completion']
			,	'resident_distinguishing_marks'=>$resident['resident_distinguishing_marks']
			,	'resident_hair_color'=>$resident['resident_hair_color']
			,	'resident_eye_color'=>$resident['resident_eye_color']
			,	'resident_body_size'=>$resident['resident_body_size']
			,	'remarks'=>(empty($complaint))? 'NO DEROGATORY RECORD': 'HAS 1 ACTIVE COMPLAINT'
			,	'purpose'=>$pcRequest['pcq_purpose']
		);

		$this->_data['form_data'] = $data;
		$this->is_secure = true;
    $this->view('clearance/format');
	}

	public function process($pcq_id){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$barangayMapper = new App\Mapper\BarangayMapper();
		$fileManagerMapper = new App\Mapper\FileManagerMapper();
		$residentAttachmentMapper = new App\Mapper\ResidentAttachmentMapper();
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$pcRequestMapper = new App\Mapper\PCRequestMapper();
		$pcRequest = $pcRequestMapper->getByFilter("pcq_id = '".$pcq_id ."'", true);

		$resident_id = $pcRequest['pcq_resident_id'];
		$resident = $residentMapper->getByFilter("resident_id = '".$resident_id."'", true);
		$basicContact = $basicContactMapper->getByFilter("bc_id = '".$resident['resident_bc_id']."'", true);
		$address = $addressMapper->getByFilter("address_id = '".$resident['resident_address_id']."'", true);
		$brgy = $barangayMapper->getByFilter("brgy_id = '".$address['address_brgy_id']."'", true);
		$residentAttachment = $residentAttachmentMapper->getByFilter("rattachment_resident_id = '".$resident_id."'");

		$data = array(
				'pcq_id'=>$pcq_id
			,	'resident_id'=>$resident_id
			,	'resident_first_name' => $basicContact['bc_first_name']
			,	'resident_middle_name' => $basicContact['bc_middle_name']
			,	'resident_last_name' => $basicContact['bc_last_name']
			,	'resident_name_ext' => $basicContact['bc_name_ext']
			,	'add_desc'	=> $address['address_desc']
			,	'address_brgy' => array(
					'brgy_id'	=> $brgy['brgy_id']
				,	'brgy_name'=> $brgy['brgy_name']
				)
			,	'resident_gender'=>$basicContact['bc_gender']
			,	'resident_birthday' => date("m/d/Y", strtotime($resident['resident_birthdate']))
			,	'resident_civil_status'=>$resident['resident_civil_status']
			,	'resident_nationality'=>$resident['resident_nationality']
			,	'phone_number_1'=>$basicContact['bc_phone_num1']
			,	'phone_number_2'=>$basicContact['bc_phone_num2']
			,	'resident_add_info'=>$resident['resident_add_info']
			,	'resident_attachment'=>$residentAttachment
			,	'resident_nickname'=>$resident['resident_nickname']
			,	'resident_occupation'=>$resident['resident_occupation']
			,	'resident_religion'=>$resident['resident_religion']
			,	'resident_yrs_community'=>$resident['resident_yrs_community']
			,	'resident_weight'=>$resident['resident_weight']
			,	'resident_height'=>$resident['resident_height']
			,	'resident_completion'=>$resident['resident_completion']
			,	'resident_distinguishing_marks'=>$resident['resident_distinguishing_marks']
			,	'resident_hair_color'=>$resident['resident_hair_color']
			,	'resident_eye_color'=>$resident['resident_eye_color']
			,	'resident_body_size'=>$resident['resident_body_size']
		);

		$this->_data['form_data'] = $data;
		$this->is_secure = true;
    	$this->view('clearance/process');
	}

	public function format_process(){

		if(!empty($_FILES)){
			$this->load->model('FileManagement/Upload_Model');
			if($_FILES['pc-img']["error"] == 0){
				$result = $this->Upload_Model->upload_profile_image($_FILES, array());
				$input = array();
				$input['params'] = $_POST;
				$input['img_dir'] = $result;
				$data = array();

				$html = $this->load->view('report/barangay_clearance', $input, true);

		    $this->load->library('MPdf');
		    //echo $html;
				$this->mpdf->generate(
					array(	'format'=>'Half Letter',
							'orientation'=>'L',
							'margin_left'=>'0',
							'margin_right'=>'0',
							'margin_top'=>'0',
							'margin_bottom'=>'0',
							'margin_head'=>'0',
							'margin_foot'=>'0',
							'html'=>$html
					));
			}
			else{
				echo "failed";
			}
		};


		//echo json_encode($_POST);
	}

	public function request_pc(){
		$alert = array(
				'message'=>''
			,	'type'=>''
			,	'reload'=>'false'
		);
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$pcRequestMapper = new App\Mapper\PCRequestMapper();
		$barangayClearanceMapper = new App\Mapper\BarangayClearanceMapper();
		$this->is_secure = false;

		if(!empty($_POST)){
			$entry = $_POST;
			$resident_id = 0;
			$filter = array(
					'bc_first_name' => $entry['resident-first-name']
				,	'bc_middle_name'=> $entry['resident-middle-name']
				,	'bc_last_name'=> $entry['resident-last-name']
				,	'bc_name_ext'=> $entry['resident-name-ext']
				,	'bc_gender'=>	$entry['resident-gender']
				,	'resident_birthdate'=>	$entry['resident-birthday']
			);
			$resident = $residentMapper->checkIfRegistered($filter, 0);
			if(empty($resident)){
				$alert['message'] = "<h2><i class=\"fa fa-exclamation\"></i> You are not registered. Visit your designated Barangay for registration</h2>";
				$alert['type'] = 'danger';
				//$this->createResident($entry);
			}
			else{
				if(count($resident) > 1){
					$resident = $residentMapper->checkIfRegistered($filter, $entry['current-barangay']);
				}
				$resident = $resident[0];
				$resident_id = $resident['resident_id'];
				if($entry['current-barangay'] == $resident['address_brgy_id']){
					//Means same requestor
					$barangayClearance = $barangayClearanceMapper->checkExistByResident($resident_id);
					if(!empty($barangayClearance)){

						$alert['reload'] = true;
						//Means has barangay Clearance
						$pcRequest = $pcRequestMapper->checkExistByResident($resident_id);
						if(empty($pcRequest)){
							$pcq_id = $pcRequestMapper->insert(array(
								'pcq_resident_id'=>$resident_id
							,	'pcq_request_date'=>date('Y-m-d H:i:s', strtotime($_POST['target-date']. ' '.$_POST['target-time'].':00'))
							,	'pcq_purpose'=>$_POST['purpose']
							,	'pcq_stage'=>'requested'
							,	'pcq_status'=>0
							,	'pcq_no'=>''
							));
							$pcq_no = $pcRequestMapper->generate_pcq_no($pcq_id);
							$pcRequestMapper->update(
								array('pcq_no'=>$pcq_no), 'pcq_id = "'.$pcq_id.'"'
							);

							$this->set_alert(array(
								'message'=>"<h2><i class=\"fa fa-check-circle-o\"></i> You may now visit the police station with your Queue No. <h1>".$pcq_no."</h1></h2>"
							,	'type'=>'success'
							));

							$alert['message'] = "<h2><i class=\"fa fa-check-circle\"></i> Request has been successfully sent</h2>";
							$alert['type'] = 'success';
						}
						else{
							//There is an ongoing request
							$this->set_alert(array(
								'message'=>"<h2><i class=\"fa fa-exclamation\"></i> An Existing Request has been found! Your Queue no. is <u>" + $pcRequest['pcq_no']+ "</u></h2>"
							,	'type'=>'warning'
							));
						}
					}
					else{
						$alert['message'] = "<h2><i class=\"fa fa-exclamation\"></i> You have an no valid Barangay Clearance, Visit your designated Barangay to request one.</h2>";
						$alert['type'] = 'danger';
					}
				}
				else{
					$alert['message'] = "<h2><i class=\"fa fa-exclamation\"></i> You are not registered. Visit your designated Barangay for registration</h2>";
					$alert['type'] = 'danger';
				}
				// $resident_id = $resident['']
			}
		}

		echo json_encode($alert);
	}

	public function request(){

		$this->is_secure = false;
		$this->_template = 'templates/public';
		$this->view('clearance/request');
	}

	private function createResident($data){
		$basicContactMapper = new App\Mapper\BasicContactMapper();
		$addressMapper = new App\Mapper\AddressMapper();
		$residentMapper = new App\Mapper\ResidentMapper();
		$bc_id = $basicContactMapper->insert(array(
				'bc_first_name' => $data['resident-first-name']
			,	'bc_middle_name'=> $data['resident-middle-name']
			,	'bc_last_name'=> $data['resident-last-name']
			,	'bc_name_ext'=> $data['resident-name-ext']
			,	'bc_phone_num1'=> $data['phone-number-1']
			,	'bc_phone_num2'=> $data['phone-number-2']
			,	'bc_phone_num3'=> ''
			,	'bc_gender'=>	$data['resident-gender']
		));

		$resident_address_id = $addressMapper->insert(array(
			'address_city_id' 		=> $this->_default_city_id
		,	'address_province_id' => $this->_default_province_id
		,	'address_brgy_id' => $data['current-barangay']
		,	'address_desc' => $data['add-desc']
		));

		$resident_id = $residentMapper->insert(array(
			'resident_bc_id'	=> $bc_id
		,	'resident_address_id'	=> $resident_address_id
		,	'resident_birthdate'	=> date("Y-m-d", strtotime($data['resident-birthday']))
		,	'resident_civil_status'	=> $data['resident-civil-status']
		,	'resident_nationality'	=> $data['resident-nationality']
		,	'resident_added_by' => 0
		,	'resident_added_time'=>  date('Y-m-d H:i:s', time())
		,	'resident_modified_by' => 0
		,	'resident_modified_time'=>  date('Y-m-d H:i:s', time())
		,	'resident_add_info'=> ''
		,	'resident_nickname'=> $data['resident-alias']
		,	'resident_occupation'=> $data['resident-occupation']
		,	'resident_religion'=> $data['resident-religion']
		,	'resident_yrs_community'=> $data['resident-yrs-community']
		,	'resident_weight'=> $data['resident-weight']
		,	'resident_height'=> $data['resident-height']
		,	'resident_completion'=> $data['resident-completion']
		,	'resident_distinguishing_marks'=> $data['resident-distinguishing-marks']
		,	'resident_hair_color'=> $data['resident-hair-color']
		,	'resident_eye_color'=> $data['resident-eye-color']
		,	'resident_body_size'=> $data['resident-body-size']
		));
	}


}
