$(document).ready(function(){
  $('#print-process').click(function(){
    let resident_id = $(this).val();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: global.site_name + 'resident/print',
        data : {
          'resident': resident_id
        },
        success : function(result){
          console.log(result)
           var win = window.open(global.site_name + 'upload/pdf/'+result.pdf_name, '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }

        },
        error: function (request, status, error) {

        }
    });
  });

  $('#print-format').click(function(){
    var win = window.open(global.site_name + 'clearance/format/'+$(this).val(), '_blank');
     if (win) {
         //Browser has allowed it to be opened
         win.focus();
     } else {
         //Browser has blocked it
         alert('Please allow popups for this website');
     }
  });

  $('.save').click(function(){
    let params = {
      'method'  : $(this).val(),
      'pc_id' : $('#police-clearance-id').val()
    };
    let message = "";
    if(params.method == 'mark-finish'){
      message = "Are you done with this police clearance request record?"
    }
    else{
      message = "Are you sure you want to delete this police clearance request record?"
    }

    bootbox.confirm({
        message: message,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
          if(result){
            $.ajax({
              url :  global.site_name + 'clearance/process-clearance' ,
              type : 'POST',
              data : params,
              success : function(result){
                bootbox.alert("<p class='text-center'>Process has been successful</p>");
                window.location.replace(global.site_name + 'clearance/request-list');
              },
              error: function (xhr, ajaxOptions, thrownError) {
                  bootbox.alert("Something went wrong!");
                  //alert(xhr.status);
                  //alert(thrownError);
              }
            })
          }

        }
    });


  });


}).on("keypress", "form", function(event) {
  return event.keyCode != 13;
});
