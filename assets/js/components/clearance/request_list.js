$(document).ready(function(){

  $('#police-clearance-request-list').DataTable( {
      responsive: true,
      processing: true,
      serverSide: true,
      bSort: true,
      ajax: {
        url : global.site_name + 'clearance/request_ref',
        type : 'POST',
        dataType : 'json',
        data : function(params){
          return params;
        },
        dataSrc: function(result){
          // let format = {
          //   data : result.data
          // , recordsTotal:result.count
          // };
          result.recordsTotal = result.total_count;
          result.recordsFiltered = result.count;

          return result.data;
        },
        cache: true
      },
      columnDefs: [ {
        //This is for the custom button
          targets: -1,
          data: "id",
          render: function ( data, type, row, meta ) {
            let id = data;
            let html = '<div class="text-center">';
            if(row['pcq_status'] == '0' || row['pcq_status'] == '1'){
              html += '<a class="btn btn-primary btn-sm has-tooltip process" title="Process" href="' + global.site_name + 'clearance/process/' + id  + '"><i class="fa fa-folder-open"></i></button>';
            }
            html +=  '</div>';
            return html;
          }
      }, ''],
      initComplete: function(){
      },
      createdRow: function( row, data, dataIndex){
          console.log(data)
          if( data['complaint'] > 0){
              $(row).addClass('danger');
          }
      },
      processing : function( e, settings, processing ) {
        //Loading animation here
        //$('#processingIndicator').css( 'display', processing ? 'block' : 'none' );
      },
      // fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      //   console.log("Event Added")
      // },
      fnDrawCallback: function (oSettings) {
        $('.delete-row').click(function(){
          // let params = config.page_var;
          // params['id'] = $(this).val();
          // bootbox.confirm({
          //     title: "Delete row",
          //     message: "Are you sure you want to delete this?",
          //     buttons: {
          //         cancel: {
          //             label: '<i class="fa fa-times"></i> Cancel'
          //         },
          //         confirm: {
          //             label: '<i class="fa fa-check"></i> Confirm'
          //         }
          //     },
          //     callback: function (result) {
          //       if(result){
          //         $.ajax({
          //           url : config.delete_url,
          //           type : 'POST',
          //           dataType : 'json',
          //           data : params,
          //           success : function(){
          //             bootbox.alert("Delete Successful");
          //             datatable.ajax.reload();
          //           },
          //           error: function (xhr, ajaxOptions, thrownError) {
          //               bootbox.alert("Something went wrong!");
          //               //alert(xhr.status);
          //               //alert(thrownError);
          //           }
          //         })
          //       }
          //     }
          // });

        });

      },
      dom: 'lfrtip',
      buttons: [
          {
              text: 'ADD',
              action: function ( e, dt, node, config ) {

              }
          }
      ],
      order:[[0,'asc']],
      columns: [
        {"data":"pcq_no"},
        { "data": "resident" },
        { "data": "pcq_purpose" },
        { "data": "brgy_name" },
        {   "data": "bc_gender"
          ,   "render": function(data, type, full, meta) {
              return '<span class="text-uppercase">' + full['bc_gender'] + ' / ' + full['age'] + ' Years Old </span>';
          }
        },
        { "data": "complaint" },
        { "data": "pcq_status"
          ,   "render": function(data, type, full, meta) {
            let status = "";
            switch(data){
              case 0:
                status = "New";
              break;
              case 1:
                status = "View";
              break;
              case 2:
                status = "Rejected";
              break;
              case 3:
                status = "Processed";
              break;
            }
            return '<span class="text-uppercase">' + status + ' </span>';
          }
        },


        { "data": "pcq_id"
        , "searchable": false}
      ]
  });



    // let config = {
    //   url : global.site_name + 'resident/resident_ref',
    //   order_col : 0,
    //   req_data : {type : 'city'},
    //   column :[
    //     { "data": "brgy_name" },
    //     { "data": "resident" },
    //     {   "data": "bc_gender"
    //       ,   "render": function(data, type, full, meta) {
    //           return full['bc_gender'] + ' / ' + full['age'];
    //       }
    //     },
    //     { "data": "resident_nationality"
    //       ,   "render": function(data, type, full, meta) {
    //           return full['resident_nationality'] + ' / ' + full['resident_civil_status'];
    //       }
    //     },
    //     { "data": "bc_phone_num1"
    //       ,   "render": function(data, type, full, meta) {
    //           return full['bc_phone_num1'] + ' / ' + full['bc_phone_num2'];
    //       }
    //     },
    //     { "data": "legal_case" },
    //     { "data": "resident_id"
    //     , "searchable": false}
    //   ],
    //   add_url : global.site_name + 'resident/add-resident',
    //   edit_url  : global.site_name + 'resident/edit-resident',
    //   delete_url : global.site_name + 'resident/delete-resident',
    //   page_var : {
    //     type : 'city'
    //   }
    // }
    // helper.datatable_basic('#resident-list', config);

});
