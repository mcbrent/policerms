$(document).ready(function(){

  $('.save-file').click(function(){
    let params = {};
    params['id'] = $(this).val();

    $.ajax({
      url :  global.site_name + 'resident/save-file' ,
      type : 'POST',
      dataType : 'json',
      data : params,
      success : function(result){
        window.open(
          global.site_name + 'upload/temp/' + result.file_name,
          '_blank');
      },
      error: function (xhr, ajaxOptions, thrownError) {
          bootbox.alert("Something went wrong!");
          //alert(xhr.status);
          //alert(thrownError);
      }
    })


  });

}).on("keypress", "form", function(event) {
  return event.keyCode != 13;
});
