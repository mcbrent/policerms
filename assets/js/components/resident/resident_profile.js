$(document).ready(function(){
    $('#next-basic-info').click(function(){
        // $('.nav-tabs a[href="#tab-additional-info"]').tab('show');
    });

  $("#main-form").validate({
      rules : {
          'resident-first-name' : {
            required : true
          }
        ,  'resident-middle-name' : {
            required : true
          }
        ,  'resident-last-name' : {
            required : true
          }
        ,  'add-desc' : {
            required : true
          }
        ,  'current-barangay' : "required"
        ,  'resident-gender' : {
            required : true
          }
        ,  'resident-birthday' : {
            required : true,
            date: true
          }
        , 'resident-civil-status' : {
            required : true
          }
        , 'resident-nationality' : {
            required : true
          }
        , 'phone-number-1' : {
            required : true
          }
        , 'resident-yrs-community' : {
            required : true
          , number: true
          }
        , 'resident-weight' : {
            required : true
          , number: true
          }
        , 'resident-height' : {
            required : true
          , number: true
          }
      },
      messages : {
        'current-barangay' : "Select Barangay"
      , 'phone-number-1' : "At least 1 phone number is required"
      }
 });

 $('.add-attachment').click(function(){
   let attachment_row = '<tr>' +
     '<td class="col-md-6 text-center">' +
       '<input name="file-name[]"  type="text" class="form-control" placeholder="Enter File Tag Name" value=""/>' +
     '</td>' +
     '<td class="col-md-4 text-center"></td>' +
     '<td class="col-md-2 text-center">' +
       '<input type="file" name="file-attachment[]">' +
     '</td>' +
   '</tr>';

   $('#attachment-table tbody').append(attachment_row);
 });

  $('[name=current-barangay]').select2({
    allowClear: true,
    ajax:{
      url: global.site_name + 'reference/ref',
      dataType: 'json',
      type:'post',
      data: function(params){
          let search = $.isEmptyObject(params)? '' : params.term;
          let option = {
            columns :[
              {
                "data": "brgy_name"
              , "searchable": true },
              {
                "data": "brgy_id"
              , "searchable": false }
            ],
            order : [
              {
                'column' : 0,
                'dir'   : 'asc'
              }
            ],
            search : {
              'value' : search
            , 'regex' : false
            },
            option : {
              'type' : 'brgy'
            },
            length : 25,
            condition:[]
          };
          option.condition.push({
              column  : 'city_id'
            , value   : '447'
          });
          return option;
      },
      processResults: function (data) {
      // Tranforms the top-level key of the response object from 'items' to 'results'
        let result = [];
        data['data'].forEach(function(d){
          result.push({
            id  : d.brgy_id
          , text: d.brgy_name
          });
        });
        return {
          results: result
        }
      }
    }
  });

  $('.delete-row').click(function(){
    let params = {};
    let current_row = $(this).closest('tr');
    params['id'] = $(this).val();
    bootbox.confirm({
        title: "Delete row",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancel'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirm'
            }
        },
        callback: function (result) {
          if(result){
            $.ajax({
              url :  global.site_name + 'resident/delete-file' ,
              type : 'POST',
              dataType : 'json',
              data : params,
              success : function(){
                bootbox.alert("Delete Successful");
                current_row.remove();
              },
              error: function (xhr, ajaxOptions, thrownError) {
                  bootbox.alert("Something went wrong!");
                  //alert(xhr.status);
                  //alert(thrownError);
              }
            })
          }
        }
    });

  });

  $('.save-file').click(function(){
    let params = {};
    params['id'] = $(this).val();

    $.ajax({
      url :  global.site_name + 'resident/save-file' ,
      type : 'POST',
      dataType : 'json',
      data : params,
      success : function(result){
        window.open(
          global.site_name + 'upload/temp/' + result.file_name,
          '_blank');
      },
      error: function (xhr, ajaxOptions, thrownError) {
          bootbox.alert("Something went wrong!");
          //alert(xhr.status);
          //alert(thrownError);
      }
    })


  });

}).on("keypress", "form", function(event) {
  return event.keyCode != 13;
});
