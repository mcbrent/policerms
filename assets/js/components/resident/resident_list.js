$(document).ready(function(){

  $('[name=barangay-list]').on("select2:select", function(e) {
      console.log("Change")
     $('#resident-list').DataTable().ajax.reload();
  });

  $('#resident-list').DataTable( {
      responsive: true,
      processing: true,
      serverSide: true,
      bSort: true,
      ajax: {
        url : global.site_name + 'resident/resident_ref',
        type : 'POST',
        dataType : 'json',
        data : function(params){
          params['filter'] = {
            'brgy' : $('[name=barangay-list]').val()
          };
          return params;
        },
        dataSrc: function(result){
          // let format = {
          //   data : result.data
          // , recordsTotal:result.count
          // };
          result.recordsTotal = result.total_count;
          result.recordsFiltered = result.count;

          return result.data;
        },
        cache: true
      },
      columnDefs: [ {
        //This is for the custom button
          targets: -1,
          data: "id",
          render: function ( data, type, row, meta ) {
            // return '';
            let id = data;
            let html = '';
            if($('#resident-list').data('view-method') == '0'){
                html = '<div class="text-center"><a class="btn btn-sm btn-success has-tooltip" title="View" href="' + global.site_name + 'resident/view-resident/' + id  + '"><i class="fa fa-search"></i></a> ' +
                '<a class="btn btn-sm btn-info has-tooltip" title="Edit" href="' + global.site_name + 'resident/edit-resident/' + id  + '"><i class="fa fa-pencil"></i></a> ' +
                '<button class="btn btn-sm btn-danger has-tooltip delete-row" title="Delete" value="' + id + '"><i class="fa fa-trash"></i></button></div>';
            }
            else{
                html = '<div class="text-center"><a class="btn btn-sm btn-success has-tooltip" title="View" href="' + global.site_name + 'resident/view-resident/' + id  + '"><i class="fa fa-search"></i></a> ' +
                '</div>';
            }
            return html;
          }
      }, ''],
      initComplete: function(){
          if($('#resident-list').data('view-method') == '0'){
              let toolbar = '<div class="pull-right"><a class="btn btn-default" role="button" href="' + global.site_name + 'resident/add-resident' + '"><i class="fa fa-file">&nbsp</i> ADD</a></div>';;
              $("div.dt-toolbar").html(toolbar);
          }

      },
      processing : function( e, settings, processing ) {
        //Loading animation here
        //$('#processingIndicator').css( 'display', processing ? 'block' : 'none' );
      },
      // fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      //   console.log("Event Added")
      // },
      fnDrawCallback: function (oSettings) {
        $('.delete-row').click(function(){
          let params = {};
          params['id'] = $(this).val();
          bootbox.confirm({
              title: "Delete Resident",
              message: "Are you sure you want to delete this? Deleting resident will delete all information associated to this resident",
              buttons: {
                  cancel: {
                      label: '<i class="fa fa-times"></i> Cancel'
                  },
                  confirm: {
                      label: '<i class="fa fa-check"></i> Confirm'
                  }
              },
              callback: function (result) {
                if(result){
                  $.ajax({
                    url : global.site_name + 'resident/delete-resident',
                    type : 'POST',
                    dataType : 'json',
                    data : params,
                    success : function(){
                      bootbox.alert("Delete Successful");
                      $('#resident-list').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        bootbox.alert("Something went wrong!");
                        //alert(xhr.status);
                        //alert(thrownError);
                    }
                  })
                }
              }
          });

        });

      },
      dom: 'l<"dt-toolbar">frtip',
      buttons: [
          {
              text: 'ADD',
              action: function ( e, dt, node, config ) {

              }
          }
      ],
      order:[[0,'asc']],
      columns: [
        { "data": "brgy_name" },
        { "data": "resident" },
        {   "data": "bc_gender"
          ,   "render": function(data, type, full, meta) {
              return full['bc_gender'] + ' / ' + full['age'];
          }
        },
        { "data": "resident_nationality"
          ,   "render": function(data, type, full, meta) {
              return full['resident_nationality'] + ' / ' + full['resident_civil_status'];
          }
        },
        { "data": "bc_phone_num1"
          ,   "render": function(data, type, full, meta) {
              return full['bc_phone_num1'] + ' / ' + full['bc_phone_num2'];
          }
        },
        { "data": "legal_case" },
        { "data": "resident_id"
        , "searchable": false}
      ]
  });



    // let config = {
    //   url : global.site_name + 'resident/resident_ref',
    //   order_col : 0,
    //   req_data : {type : 'city'},
    //   column :[
    //     { "data": "brgy_name" },
    //     { "data": "resident" },
    //     {   "data": "bc_gender"
    //       ,   "render": function(data, type, full, meta) {
    //           return full['bc_gender'] + ' / ' + full['age'];
    //       }
    //     },
    //     { "data": "resident_nationality"
    //       ,   "render": function(data, type, full, meta) {
    //           return full['resident_nationality'] + ' / ' + full['resident_civil_status'];
    //       }
    //     },
    //     { "data": "bc_phone_num1"
    //       ,   "render": function(data, type, full, meta) {
    //           return full['bc_phone_num1'] + ' / ' + full['bc_phone_num2'];
    //       }
    //     },
    //     { "data": "legal_case" },
    //     { "data": "resident_id"
    //     , "searchable": false}
    //   ],
    //   add_url : global.site_name + 'resident/add-resident',
    //   edit_url  : global.site_name + 'resident/edit-resident',
    //   delete_url : global.site_name + 'resident/delete-resident',
    //   page_var : {
    //     type : 'city'
    //   }
    // }
    // helper.datatable_basic('#resident-list', config);

});
