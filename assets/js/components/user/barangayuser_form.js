$(document).ready(function(){
  if($("#main-form").data('action') == 'add'){
    $("#main-form").validate({
      rules :{
        'current-barangay': {
          required : true
        },
        'brgy-position': {
          required : true
        },
        'first-name': {
          required : true
        },
        'last-name': {
          required : true
        },
        'gender':{
          required : true
        },
        'user-email': {
          required : true,
          email : true
        },
        'user-id':{
          required : true,
          minlength: 6
        },
        'user-password':{
          required : true,
          minlength: 6
        },
        'user-re-password':{
          required : true,
          equalTo : "[name=user-password]"
        }
      }

    });
  }
  else{
    $("#main-form").validate({
      rules :{
        'current-barangay': {
          required : true
        },
        'brgy-position': {
          required : true,
          maxlength:20
        },
        'first-name': {
          required : true
        },
        'last-name': {
          required : true
        },
        'gender':{
          required : true
        },
        'user-email': {
          required : true,
          email : true
        },
        'user-id':{
          required : true,
          minlength: 6
        },
        'user-re-password':{
          equalTo : "[name=user-password]"
        }
      }

    });
  }



  $('[name=current-barangay]').select2({
    allowClear: true,
    ajax:{
      url: global.site_name + 'reference/ref',
      dataType: 'json',
      type:'post',
      data: function(params){
          let search = $.isEmptyObject(params)? '' : params.term;
          let option = {
            columns :[
              {
                "data": "brgy_name"
              , "searchable": true },
              {
                "data": "brgy_id"
              , "searchable": false }
            ],
            order : [
              {
                'column' : 0,
                'dir'   : 'asc'
              }
            ],
            search : {
              'value' : search
            , 'regex' : false
            },
            option : {
              'type' : 'brgy'
            },
            length : 100,
            condition:[]
          };
          option.condition.push({
              column  : 'city_id'
            , value   : '447'
          });
          return option;
      },
      processResults: function (data) {
      // Tranforms the top-level key of the response object from 'items' to 'results'
        let result = [];
        data['data'].forEach(function(d){
          result.push({
            id  : d.brgy_id
          , text: d.brgy_name
          });
        });
        return {
          results: result
        }
      }
    }
  });
});
