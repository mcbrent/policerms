$(document).ready(function(){
  if($("#main-form").data('action') == 'add'){
    $("#main-form").validate({
      rules :{
        'first-name': {
          required : true
        },
        'last-name': {
          required : true
        },
        'gender':{
          required : true
        },
        'user-email': {
          required : true,
          email : true
        },
        'user-id':{
          required : true,
          minlength: 6
        },
        'user-password':{
          required : true,
          minlength: 6
        },
        'user-re-password':{
          required : true,
          equalTo : "[name=user-password]"
        }
      }

    });
  }
  else{
    $("#main-form").validate({
      rules :{
        'first-name': {
          required : true
        },
        'last-name': {
          required : true
        },
        'gender':{
          required : true
        },
        'user-email': {
          required : true,
          email : true
        },
        'user-id':{
          required : true,
          minlength: 6
        },
        'user-re-password':{
          equalTo : "[name=user-password]"
        }
      }

    });
  }





});
