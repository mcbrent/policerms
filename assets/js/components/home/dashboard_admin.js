$(document).ready(function(){
  update();
});





let update = function(){

  $.ajax({
      type: 'POST',
      dataType: 'json',
      url: global.site_name + 'home/dashboard-catch',
      data : {
        today :  moment().format('M/D/YYYY')
      },
      success : function(result){
        let sched_police_clearance = result.sched_police_clearance;
        $('#sched-pc .content-value').text(sched_police_clearance.processed + ' / ' + sched_police_clearance.total);
        $('#sched-pc .content-percent').text(sched_police_clearance.percentage);

      },
      error: function (request, status, error) {

      }
  });
}
