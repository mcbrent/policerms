$(document).ready(function(){

  $('#resident-list').DataTable( {
      responsive: true,
      processing: true,
      serverSide: true,
      bSort: true,
      ajax: {
        url : global.site_name + 'brgyclearance/brgyclearance-ref',
        type : 'POST',
        dataType : 'json',
        data : function(params){
          return params;
        },
        dataSrc: function(result){
          // let format = {
          //   data : result.data
          // , recordsTotal:result.count
          // };
          result.recordsTotal = result.total_count;
          result.recordsFiltered = result.count;

          return result.data;
        },
        cache: true
      },
      columnDefs: [ {
        //This is for the custom button
          targets: -1,
          data: "id",
          render: function ( data, type, row, meta ) {
            // return '';
            let id = data;
            let html = '<div class="text-center">' +
            '<button class="btn btn-sm btn-danger has-tooltip delete-row" title="Delete" value="' + id + '"><i class="fa fa-trash"></i></button></div>'
            return html;
          }
      }, ''],
      initComplete: function(){
        let toolbar = '<div class="pull-right"><a class="btn btn-default" role="button" href="' + global.site_name + 'brgyclearance/issue' + '"><i class="fa fa-id-badge">&nbsp</i> ADD</a></div>';;
        $("div.dt-toolbar").html(toolbar);
      },
      processing : function( e, settings, processing ) {
        //Loading animation here
        //$('#processingIndicator').css( 'display', processing ? 'block' : 'none' );
      },
      // fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      //   console.log("Event Added")
      // },
      fnDrawCallback: function (oSettings) {
        $('.delete-row').click(function(){
          let params = {};
          params['id'] = $(this).val();
          bootbox.confirm({
              title: "Delete row",
              message: "Are you sure you want to delete this?",
              buttons: {
                  cancel: {
                      label: '<i class="fa fa-times"></i> Cancel'
                  },
                  confirm: {
                      label: '<i class="fa fa-check"></i> Confirm'
                  }
              },
              callback: function (result) {
                if(result){
                  $.ajax({
                    url : global.site_name + 'brgyclearance/delete',
                    type : 'POST',
                    dataType : 'json',
                    data : params,
                    success : function(){
                      bootbox.alert("Delete Successful");
                      $('#resident-list').DataTable().ajax.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        bootbox.alert("Something went wrong!");
                        //alert(xhr.status);
                        //alert(thrownError);
                    }
                  })
                }
              }
          });

        });

      },
      dom: 'l<"dt-toolbar">frtip',
      buttons: [
          {
              text: 'ADD',
              action: function ( e, dt, node, config ) {

              }
          }
      ],
      order:[[2,'desc']],
      columns: [
        { "data": "resident" },
        { "data": "brgy_name" },
        { "data": "brgyc_validity_from",
          "render":function(data, type, full, meta) {
              return full['brgyc_validity_from'] + ' - ' + full['brgyc_validity_to'];
          }
        },
        { "data": "validity",
          "render":function(data, type, full, meta) {
              return (full['validity'] == '1')? 'Valid' : 'Expired';
          }
        }
      ]
  });



    // let config = {
    //   url : global.site_name + 'resident/resident_ref',
    //   order_col : 0,
    //   req_data : {type : 'city'},
    //   column :[
    //     { "data": "brgy_name" },
    //     { "data": "resident" },
    //     {   "data": "bc_gender"
    //       ,   "render": function(data, type, full, meta) {
    //           return full['bc_gender'] + ' / ' + full['age'];
    //       }
    //     },
    //     { "data": "resident_nationality"
    //       ,   "render": function(data, type, full, meta) {
    //           return full['resident_nationality'] + ' / ' + full['resident_civil_status'];
    //       }
    //     },
    //     { "data": "bc_phone_num1"
    //       ,   "render": function(data, type, full, meta) {
    //           return full['bc_phone_num1'] + ' / ' + full['bc_phone_num2'];
    //       }
    //     },
    //     { "data": "legal_case" },
    //     { "data": "resident_id"
    //     , "searchable": false}
    //   ],
    //   add_url : global.site_name + 'resident/add-resident',
    //   edit_url  : global.site_name + 'resident/edit-resident',
    //   delete_url : global.site_name + 'resident/delete-resident',
    //   page_var : {
    //     type : 'city'
    //   }
    // }
    // helper.datatable_basic('#resident-list', config);

});
