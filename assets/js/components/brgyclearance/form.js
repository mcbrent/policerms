$(document).ready(function(){

  $("#main-form").validate({
    rules : {
        'resident-id' : {
          required : true
        }
      ,  'valid-from' : {
          required : true,
          date: true
        }
      ,  'valid-to' : {
          required : true,
          date: true
        }
    }
   });

  $('#view-resident').click(function(){
    let resident_id = $('[name=resident-id]').val();
    if(resident_id != '0'){
      var win = window.open(global.site_name + 'resident/view-resident/' + resident_id, '_blank');
      win.focus();
    }
    else{
      bootbox.alert("No Resident Selected!");
    }

  });



  $('[name=resident-id]').select2({
    allowClear: true,
    ajax:{
      url: global.site_name + 'resident/resident-ref',
      dataType: 'json',
      type:'post',
      data: function(params){
          let search = $.isEmptyObject(params)? '' : params.term;
          let option = {
            columns :[
              {
                "name" : 'resident'
              , "data": "resident"
              , "searchable": true },
              { "name" : 'resident_id'
              , "data": "resident_id"
              , "searchable": false}
            ],
            order : [
              {
                'column' : 1,
                'dir'   : 'asc'
              }
            ],
            search : {
              'value' : search
            , 'regex' : false
            },
            option : {

            },
            length : 25,
            start: 0

          };
          return option;
      },
      processResults: function (data) {
      // Tranforms the top-level key of the response object from 'items' to 'results'
        let result = [];
        data['data'].forEach(function(d){
          result.push({
            id  : d.resident_id
          , text: d.resident + ' - ' + d.age + ' years old ( Brgy. ' + d.brgy_name + ') '
          });
        });
        return {
          results: result
        }
      }
    }
  });
});
