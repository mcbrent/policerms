$(document).ready(function(){


    let config = {
      url : global.site_name + 'reference/ref',
      order_col : 3,
      req_data : {type : 'barangay'},
      column :[
        { "data": "region_desc" },
        { "data": "province_name" },
        { "data": "city_name" },
        { "data": "brgy_name" },
        { "data": "brgy_id"
        , "searchable": false}
      ],
      add_url : global.site_name + 'reference/add-barangay',
      edit_url  : global.site_name + 'reference/edit-barangay',
      delete_url : global.site_name + 'reference/delete-ref',
      page_var : {
        type : 'city'
      }
    }
    helper.datatable_basic('.datatable-basic', config);

});
